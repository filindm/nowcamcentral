FROM python:2.7.13
COPY app /srv/app
COPY run.py /srv/run.py
COPY config.py /srv/config.py
COPY requirements.txt /srv/requirements.txt
WORKDIR /srv
RUN pip install -r requirements.txt
CMD python run.py
EXPOSE 5000
