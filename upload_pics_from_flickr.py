import requests
import StringIO
import argparse
import string
import random


AUTH_HDRS = {'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6IjU3MmNhMzNhM2Q2ZWU5MTc5MGIxZTRlZiIsImlhdCI6MTQ2NTI2MDcxMSwibmJmIjoxNDY1MjYwNzExLCJleHAiOjE2MjI5NDA3MTF9.S5Zm2T0EfANXnC3_MfizrtYIZIWMDQvmmwtAkexwn44'}
# localhost:5000:
# AUTH_HDRS = {'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6IjU3NGZkOGQ5OGE4NDhhZjc1Y2I0MTc5OSIsImlhdCI6MTQ2NTI2MDAxNCwibmJmIjoxNDY1MjYwMDE0LCJleHAiOjE0NjUyNjM2MTR9.EiiPifEJnlZDL_b9anwKz_c8Atz9WI_ryNZrwCxFBFQ'}


def gen_photo_code():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))


def upload_photo(host, name, file, event_id):
    url = '{}/photos'.format(host)
    data = {
        'name': name,
        'event': event_id,
        'code': gen_photo_code()
    }
    files = {
        'file': file
    }
    print 'uploading to {}'.format(url)
    r = requests.post(url, data=data, files=files, verify=False, headers=AUTH_HDRS)
    print r.text


def upload_photos(host, event_id, photos_num):
    r = requests.get(
        'https://api.flickr.com/services/rest/',
        params={
            'format': 'json',
            'nojsoncallback': 1,
            'method': 'flickr.interestingness.getList',
            'api_key': '7617adae70159d09ba78cfec73c13be3',
            'per_page': photos_num,
            'page': 1
        })
    r = r.json()
    if r['stat'] != 'ok':
        raise Exception('Flickr returned error: %s' % r['message'])

    i = 0
    for p in r['photos']['photo']:
        i = i + 1
        url = 'http://farm%(farm_id)s.staticflickr.com/%(server_id)s/%(id)s_%(secret)s_b.jpg' % {
            'farm_id': p['farm'],
            'server_id': p['server'],
            'id': p['id'],
            'secret': p['secret']
        }
        # print p
        # print 'Title: %s' % title
        # print 'URL: %s' % url
        # print
        try:
            print 'downloading {}'.format(url)
            f = StringIO.StringIO(requests.get(url).content)
            upload_photo(host=host, name=p['title'], file=f, event_id=event_id)
        except Exception as ex:
            f.close()
            print ex

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', required=False, metavar='host', default='http://localhost:5000')
    parser.add_argument('--event', required=True, metavar='event_id')
    # parser.add_argument('--unit', required=True, metavar='unit_id')
    parser.add_argument('--photos-number', required=False, metavar='photos_number', default=30)
    # parser.add_argument('--ssl', dest='ssl', action='store_true')
    # parser.add_argument('--no-ssl', dest='ssl', action='store_false')
    # parser.set_defaults(ssl=False)
    args = parser.parse_args()
    print '%r' % args
    upload_photos(host=args.host, event_id=args.event, photos_num=args.photos_number)

if __name__ == '__main__':
    main()







