import os
import datetime

APP_BASE_DIR = os.path.abspath(os.path.dirname(__file__))

APP_TITLE = 'Nowcam Central'
DEBUG = True
# API_ROOT_URI = '/api/v0.1'

SECRET_KEY = 'NMljDXC0ikM9L6C6ulCx'

### JWT
JWT_EXPIRATION_DELTA = datetime.timedelta(days=360*5)

### MongoDB
MONGO_DBNAME = 'nowcam'
MONGO_URL = 'localhost'

### Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(message)s'
        }
    },
    'handlers': {
        # 'console': {
        #     'class': 'logging.StreamHandler',
        #     'level': 'DEBUG',
        #     'formatter': 'simple',
        #     'stream': 'ext://sys.stdout'
        # },
        'file': {
            'class' : 'logging.handlers.RotatingFileHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'filename': 'NowcamCentral.log',
            'maxBytes': 1024 * 1024,
            'backupCount': 3
        }
    },
    'root': {
        # 'handlers': ['console', 'file'],
        'handlers': ['file'],
        'level': 'DEBUG'
    }#,
    # 'loggers': {
    #     # 'root': {
    #     #     'level': 'DEBUG',
    #     #     'handlers': ['console', 'file']
    #     # },
    #     'app.__init__': {
    #         'level': 'DEBUG',
    #         'handlers': ['console', 'file']
    #     },
    #     'app.views': {
    #         'level': 'DEBUG',
    #         'handlers': ['console', 'file']
    #     },
    # }
}
