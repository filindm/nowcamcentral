
import unittest

import app
app.app.config['MONGO_DBNAME'] = 'nowcam_test'
app.app.config['MONGO_URL'] = 'localhost'
app.app.config['TESTING'] = True
from app import init_db
init_db()
from app import views, models
from bson import ObjectId
import json


class BaseTestCase(unittest.TestCase):

    USERID   = '572ca33a3d6ee91790b1e4ef'
    USERNAME = 'test@test.com'
    PASSWORD = 'password'


    def setUp(self):
        app.db.client.drop_database(app.app.config['MONGO_DBNAME'])
        models.User.create({'_id':ObjectId(self.USERID), 'email': self.USERNAME, 'password': self.PASSWORD, 'roles': ['admin']})
        self.app = app.app.test_client()


    def tearDown(self):
        app.db.client.drop_database(app.app.config['MONGO_DBNAME'])


    def _login_user(self, **kwargs):
        rv = self.app.post('/auth', data=json.dumps({'username': kwargs['username'], 'password': kwargs['password']}), content_type='application/json')
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.headers['Content-type'], 'application/json')
        token = json.loads(rv.data)['access_token']
        self.assertNotEqual(token, '')
        return {'Authorization': 'JWT ' + token}


    def _login_admin_user(self):
        return self._login_user(username=self.USERNAME, password=self.PASSWORD)

