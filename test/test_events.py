#!env/bin/python

import sys
import os
import unittest
import json
from base import BaseTestCase
from app.models import Customer, Event, User
import logging


package_directory = os.path.dirname(os.path.abspath(__file__))


class EventsTestCase(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp(self)
        self.c1 = Customer.create({'name': 'Customer One'})
        self.c2 = Customer.create({'name': 'Customer Two'})
        self.e1 = Event.create({'name': 'Event One', 'customer': str(self.c1['_id']), 'active': True})
        id = str(self.e1['_id'])
        with open(os.path.join(package_directory, 'res', 'car.jpg')) as f:
            Event.set_microsite_logo(id, f)
            Event.set_microsite_bkg(id, f)
            Event.set_ticket_body(id, f)
            Event.set_overlay(id, f)
        self.e2 = Event.create({'name': 'Event Two', 'customer': str(self.c1['_id']), 'active': False})
        self.e3 = Event.create({'name': 'Event Three', 'customer': str(self.c2['_id']), 'active': True})
        self.guest = User.create({'email': 'guest@test.com', 'password': 'password', 'roles': []})
        self.customer = User.create({'email': 'customer@test.com', 'password': 'password', 'roles': ['customer'], 'details': {'customer': str(self.c1['_id'])}})


    def test_events_unauthorized(self):
        ''' Test that protected API methods return 401 on unauthorized access and that unprotected don't '''

        id = str(self.e1['_id']) 

        rv = self.app.get('/events')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id)
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id + '/public')
        self.assertEqual(rv.status_code, 200)
        
        rv = self.app.post('/events')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.put('/events')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.post('/events/' + id + '/microsite_logo')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id + '/microsite_logo')
        self.assertEqual(rv.status_code, 200)
        
        rv = self.app.post('/events/' + id + '/microsite_bkg')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id + '/microsite_bkg')
        self.assertEqual(rv.status_code, 200)
        
        rv = self.app.post('/events/' + id + '/ticket_body')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id + '/ticket_body')
        self.assertEqual(rv.status_code, 200)
        
        rv = self.app.post('/events/' + id + '/overlay')
        self.assertEqual(rv.status_code, 401)
        
        rv = self.app.get('/events/' + id + '/overlay')
        self.assertEqual(rv.status_code, 200)


    def test_list_events_for_guest(self):
        ''' Test that a guest user (i.e. without 'admin' or 'customer' roles) can only list events with active = True '''
        rv = self.app.get('/events', headers=self._login_user(username=self.guest['email'], password=self.guest['password']))
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data)
        self.assertEqual(len(data['items']), 2)
        ids = [ x['_id'] for x in data['items'] ]
        self.assertIn(str(self.e1['_id']), ids)
        self.assertIn(str(self.e3['_id']), ids) 
        

    def test_list_events_for_customer(self):
        ''' Test that a customer user (i.e. with 'customer' in roles) can only list his/her own events '''
        rv = self.app.get('/events', headers=self._login_user(username=self.customer['email'], password=self.customer['password']))
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data)
        self.assertEqual(len(data['items']), 2)
        ids = [ x['_id'] for x in data['items'] ]
        self.assertIn(str(self.e1['_id']), ids)
        self.assertIn(str(self.e2['_id']), ids) 


    def test_list_events_for_admin(self):
        ''' Test that an admin user (i.e. with 'admin' in roles) can list all events '''
        rv = self.app.get('/events', headers=self._login_admin_user())
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data)
        self.assertEqual(len(data['items']), 3)
        ids = [ x['_id'] for x in data['items'] ]
        self.assertIn(str(self.e1['_id']), ids)
        self.assertIn(str(self.e2['_id']), ids) 
        self.assertIn(str(self.e3['_id']), ids) 


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('abc').setLevel(logging.DEBUG)
    unittest.main()


