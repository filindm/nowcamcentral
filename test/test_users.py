#!env/bin/python

import sys
import unittest
import json
from base import BaseTestCase
import logging


class UsersTestCase(BaseTestCase):

    def test_users_unauthorized(self):
        rv = self.app.get('/users/self')
        self.assertEqual(rv.status_code, 401)
        rv = self.app.get('/users')
        self.assertEqual(rv.status_code, 401)
        rv = self.app.get('/users/123')
        self.assertEqual(rv.status_code, 401)
        rv = self.app.get('/users/rfid/123')
        self.assertEqual(rv.status_code, 401)
        rv = self.app.post('/users')
        self.assertEqual(rv.status_code, 401)
        rv = self.app.put('/users')
        self.assertEqual(rv.status_code, 401)


    def test_view_self(self):
        rv = self.app.get('/users/self', headers=self._login_admin_user())
        self.assertEqual(rv.status_code, 200)
        me = json.loads(rv.data)
        self.assertEqual(me['email'], BaseTestCase.USERNAME)


    def test_list_users(self):
        rv = self.app.get('/users', headers=self._login_admin_user())
        self.assertEqual(rv.status_code, 200)
        users = json.loads(rv.data)
        self.assertEqual(users['total'], 1)
        self.assertEqual(len(users['items']), 1)
        

    def test_get_user(self):
        rv = self.app.get('/users/' + BaseTestCase.USERID, headers=self._login_admin_user())
        self.assertEqual(rv.status_code, 200)
        user = json.loads(rv.data)
        self.assertEqual(user['email'], BaseTestCase.USERNAME)


    def test_get_user_by_rfid(self):
        self.fail()


    def test_create_user(self):
        u1 = {
            'email': 'abc@abc.com',
            'password': 'abcdef',
            'roles': [],
            'details': {
                'rfids': ['123']
            }
        }
        rv = self.app.post('/users', data=json.dumps(u1), headers=self._login_admin_user(), content_type='application/json')
        # print rv.data
        self.assertEqual(rv.status_code, 200)
        u2 = json.loads(rv.data)
        self.assertDictContainsSubset(u1, u2) # u2 must be superset of u1; what about the 'password' field?
        rv = self.app.get('/users/' + u2['_id'], headers=self._login_admin_user())
        # print rv.data
        self.assertEqual(rv.status_code, 200)
        u3 = json.loads(rv.data)
        self.assertDictContainsSubset(u1, u3) # u3 must be superset of u1; what about the 'password' field?


    def test_cannot_create_user_with_fb_id(self):
        u1 = {
            'email': 'abc@abc.com',
            'password': 'abcdef',
            'roles': [],
            'details': {
                'fb_id': '123'
            }
        }
        rv = self.app.post('/users', data=json.dumps(u1), headers=self._login_admin_user(), content_type='application/json')
        self.assertEqual(rv.status_code, 400)


    def test_update_user(self):
        u1 = {
            '_id': BaseTestCase.USERID,
            'details': {
                'rfids': ['123']
            }
        }
        # 1
        rv = self.app.put('/users', data=json.dumps(u1), headers=self._login_admin_user(), content_type='application/json')
        # print rv.data
        self.assertEqual(rv.status_code, 200)
        u2 = json.loads(rv.data)
        self.assertEqual(u1['details'], u2['details'])
        self.assertEqual(BaseTestCase.USERNAME, u2['email'])
        # 2
        u1['details']['rfids'].append('234')
        rv = self.app.put('/users', data=json.dumps(u1), headers=self._login_admin_user(), content_type='application/json')
        # print rv.data
        self.assertEqual(rv.status_code, 200)
        u2 = json.loads(rv.data)
        self.assertEqual(u1['details'], u2['details'])
        self.assertEqual(BaseTestCase.USERNAME, u2['email'])


    def test_unique_email_enforced(self):
        u = {
            'email': 'abc@abc.com',
            'password': 'abcdef'
        }
        rv = self.app.post('/users', data=json.dumps(u), headers=self._login_admin_user(), content_type='application/json')
        self.assertEqual(rv.status_code, 200)
        u['password'] = 'defabc'
        rv = self.app.post('/users', data=json.dumps(u), headers=self._login_admin_user(), content_type='application/json')
        self.assertEqual(rv.status_code, 500)
        # self.assertEqual(json.loads(rv.data)['error'], 'Email already exists')


    # def _auth_header(self):
    #     token = self._login()
    #     return {'Authorization': 'JWT ' + token}


    # def _login(self):
    #     rv = self.app.post('/auth', data=json.dumps({'username': BaseTestCase.USERNAME, 'password': BaseTestCase.PASSWORD}), content_type='application/json')
    #     self.assertEqual(rv.status_code, 200)
    #     self.assertEqual(rv.headers['Content-type'], 'application/json')
    #     token = json.loads(rv.data)['access_token']
    #     self.assertNotEqual(token, '')
    #     return token


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('abc').setLevel(logging.DEBUG)
    unittest.main()


