import unittest
import requests
# import json
import os
import threading
import pymongo
import logging
import sys
import arrow

os.environ['NOWCAM_SETTINGS'] = os.path.abspath('test.config.py')
from app import app, init_db
init_db()
from app import views, models
print 'MONGO_DBNAME:', app.config['MONGO_DBNAME']

import cherrypy
from cherrypy import wsgiserver


HOST = '127.0.0.1'
PORT = 5000
USER = 'filindm@gmail.com'
PASS = 'password'

d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
server = wsgiserver.CherryPyWSGIServer((HOST, PORT), d)
base_url = 'http://{}:{}'.format(HOST, PORT)
t = threading.Thread(target=lambda: server.start())


class MyTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._drop_db()
        pymongo.MongoClient(host=app.config['MONGO_URL'])[app.config['MONGO_DBNAME']]['users'].insert({
            'email': USER,
            'password': PASS
        })
        t.start()

    @classmethod
    def tearDownClass(cls):
        server.stop()
        cls._drop_db()

    # def setUp(self):
    #     print 'setUp'

    # def tearDown(self):
    #     print 'tearDown'

    @classmethod
    def _drop_db(cls):
        pymongo.MongoClient(host=app.config['MONGO_URL']).drop_database(app.config['MONGO_DBNAME'])

        
    def test_everything(self):
        self.check_auth()
        self.check_customers_empty()
        c_id = self.check_customer_creation()
        self.check_customer_update(c_id)
        self.check_customer_logo(c_id)
        # self.check_customer_delete(c_id) # Not implemented yet
        self.check_events_empty()
        e_id = self.check_event_create(c_id)
        self.check_event_update(e_id)
        self.check_event_microsite_logo(e_id)
        self.check_event_microsite_bkg(e_id)
        self.check_event_ticket_body(e_id)
        self.check_event_user_data(e_id)
        self.check_units_empty()
        u_id = self.check_unit_creation()
        self.check_unit_update(u_id)
        self.check_photos_empty(e_id)
        p_id = self.check_photo_creation(e_id, u_id)
        self.check_photos_bulk_update(e_id, [p_id])
        self.check_settings_empty()
        self.check_settings_update()


    def check_auth(self):
        print 'check_auth: ' + base_url + '/customers'
        r = requests.get(base_url + '/customers')
        self.assertEqual(r.status_code, 401)
        r = requests.post(base_url + '/auth', json={
            'username': USER,
            'password': PASS
        })
        print 'r.text:', r.text
        r.raise_for_status()
        self.access_token = r.json()['access_token']
        r = requests.get(base_url + '/customers', auth=JwtAuth(self.access_token))
        r.raise_for_status()


    def check_customers_empty(self):
        r = requests.get(base_url + '/customers', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(len(r.json()['items']), 0)
        self.assertEqual(r.json()['total'], 0)
        self.assertEqual(len(r.json()['events_count']), 0)

    
    def check_customer_creation(self):
    
        c1 = {
            u'name': u'Customer One',
            u'contact_name': u'Contact Name',
            u'contact_phone': u'1234567890',
            u'contact_email': u'abc.def@nowhere.org'
        }
        # c1_files = {'logo': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}
        r = requests.post(base_url + '/customers', json=c1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        # logging.getLogger(__name__).debug(r.json())
        
        c2 = r.json()
        self.assertIn('_id', c2)
        self.assertEqual(c1[u'name'], c2[u'name'])
        self.assertEqual(c1[u'contact_name'], c2[u'contact_name'])
        self.assertEqual(c1[u'contact_phone'], c2[u'contact_phone'])
        self.assertEqual(c1[u'contact_email'], c2[u'contact_email'])

        c_id = c2['_id']
        # logo_id = r.json()['logo']
        # c1[u'id'] = c_id
        # c1[u'logo'] = logo_id
        r = requests.get(base_url + '/customers/' + c_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()

        c2 = r.json()
        self.assertEqual(c1[u'name'], c2[u'name'])
        self.assertEqual(c1[u'contact_name'], c2[u'contact_name'])
        self.assertEqual(c1[u'contact_phone'], c2[u'contact_phone'])
        self.assertEqual(c1[u'contact_email'], c2[u'contact_email'])
        return c_id


    def check_customer_update(self, c_id):
        upd = {
            u'_id': c_id,
            u'contact_name': u'A Different Name'
        }
        r = requests.put(base_url + '/customers', json=upd, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/customers/' + c_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        c = r.json()
        self.assertEqual(c[u'contact_name'], upd[u'contact_name'])


    def check_customer_logo(self, c_id):
        url = base_url + '/customers/' + c_id + '/logo'
        r = requests.post(url, files={'logo': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(url, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)

    
    def check_customer_delete(self, c_id):
        r = requests.delete(base_url + '/customers/' + c_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/customers/' + c_id, auth=JwtAuth(self.access_token))
        self.assertEqual(r.status, 404)

    
    def check_events_empty(self):
        r = requests.get(base_url + '/events', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(len(r.json()['items']), 0)
        self.assertEqual(r.json()['total'], 0)


    def check_event_create(self, c_id):
        e1 = {
            u'name': u'Event One',
            u'customer': c_id
        }
        # e1_files = {'microsite_logo': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}
        r = requests.post(base_url + '/events', json=e1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        e2 = r.json()
        self.assertIn('_id', e2)
        e_id = e2['_id']
        r = requests.get(base_url + '/events/' + e_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        e2 = r.json()
        self.assertEqual(e1[u'name'], e2[u'name'])
        self.assertEqual(e1[u'customer'], e2[u'customer'])
        # self.assertIsNotNone(e2[u'microsite_logo'])
        return e_id


    def check_event_update(self, e_id):
        e1 = {
            u'_id': e_id,
            u'description': u'event description',
        }
        r = requests.put(base_url + '/events', json=e1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/events/' + e_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        e2 = r.json()
        self.assertEqual(e1[u'description'], e2[u'description'])
        # self.assertEqual(e1[u'microsite_sharing'], e2[u'microsite_sharing'])
        # self.assertEqual(e1[u'microsite_css'], e2[u'microsite_css'])


    def check_event_microsite_logo(self, e_id):
        url = base_url + '/events/' + e_id + '/microsite_logo'
        r = requests.post(url, files={'microsite_logo': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(url, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)


    def check_event_microsite_bkg(self, e_id):
        url = base_url + '/events/' + e_id + '/microsite_bkg'
        r = requests.post(url, files={'microsite_bkg': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(url, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)


    def check_event_ticket_body(self, e_id):
        url = base_url + '/events/' + e_id + '/ticket_body'
        r = requests.post(url, files={'ticket_body': open('/Users/fdm/Pictures/cars/1.jpg', 'rb')}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(url, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)


    def check_event_user_data(self, e_id):
        url = base_url + '/events/' + e_id + '/user_data'
        data1 = {
            'event': e_id,
            'user_data': {
                'abc': 'def',
            },
            'date_submitted': str(arrow.utcnow())
        }
        r = requests.post(url, json=data1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(url, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)


    # Units
    def check_units_empty(self):
        r = requests.get(base_url + '/units', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(len(r.json()['items']), 0)
        self.assertEqual(r.json()['total'], 0)

    
    def check_unit_creation(self):
        u1 = {
            u'name': u'Unit One',
            u'mac': u'12345678',
            u'event': u'1234567890',
            u'code': u'123',
            u'description': 'abc',
            u'active': True
        }
        r = requests.post(base_url + '/units', json=u1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        u_id = r.json()['_id']
        r = requests.get(base_url + '/units/' + u_id, auth=JwtAuth(self.access_token))
        u2 = r.json()
        self.assertIn('_id', u2)
        self.assertEqual(u1[u'name'], u2[u'name'])
        self.assertEqual(u1[u'mac'], u2[u'mac'])
        # self.assertEqual(u1[u'event'], u2[u'event'])
        self.assertEqual(u1[u'code'], u2[u'code'])
        self.assertEqual(u1[u'description'], u2[u'description'])
        self.assertEqual(u1[u'active'], u2[u'active'])
        return u_id


    def check_unit_update(self, u_id):
        u1 = {
            u'_id': u_id,
            u'name': u'Another Name',
            u'mac': u'87654321',
            # u'event': u'0987654321',
            u'code': u'321',
            u'description': u'cba',
            u'active': False
        }
        r = requests.put(base_url + '/units', json=u1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/units/' + u_id, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        u2 = r.json()
        self.assertEqual(u1[u'name'], u2[u'name'])
        self.assertEqual(u1[u'mac'], u2[u'mac'])
        # self.assertEqual(u1[u'event'], u2[u'event'])
        self.assertEqual(u1[u'code'], u2[u'code'])
        self.assertEqual(u1[u'description'], u2[u'description'])
        self.assertEqual(u1[u'active'], u2[u'active'])


    # Photos
    def check_photos_empty(self, event_id):
        r = requests.get(base_url + '/photos', params={'event_id': event_id}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(len(r.json()['items']), 0)
        self.assertEqual(r.json()['total'], 0)

    
    def check_photo_creation(self, event_id, unit_id):
        filepath = '/Users/fdm/Pictures/cars/1.jpg'
        p1 = {
            u'name': u'Photo One',
            u'event': event_id,
            u'unit': unit_id,
            u'code': u'123',
            u'stat': {},
            u'hidden': False
        }
        r = requests.post(base_url + '/photos', data=p1, files={'file': open(filepath, 'rb')}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        p_id = r.json()['_id']
        r = requests.get(base_url + '/photos', params={'event_id': event_id}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(len(r.json()['items']), 1)
        self.assertEqual(r.json()['total'], 1)
        r = requests.get(base_url + '/photos/' + p_id + '/image', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)
        from PIL import Image
        from io import BytesIO
        Image.open(BytesIO(open(filepath).read())).show()
        Image.open(BytesIO(r.content)).show()
        # self.assertEqual(r.content[0:100], open(filepath, 'rb').read()[0:100])
        r = requests.get(base_url + '/photos/' + p_id + '/thumbnail', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertIsNotNone(r.content)
        self.assertGreater(len(r.content), 0)
        return p_id


    # def _bin_files_equal(fp1, fp2):
    #     while True:
    #         buf1 = fp1.read(1024)
    #         buf2 = fp2.read(1024)
    #         if buf1 != buf2:
    #             return False
    #         if not buf1:
    #             return True


    def check_photos_bulk_update(self, event_id, photo_ids):
        items = [{'_id': id, 'hidden': True} for id in photo_ids]
        r = requests.put(base_url + '/photos', json={'items': items}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/photos', params={'event_id': event_id}, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        for p in r.json()['items']:
            self.assertEqual(p['hidden'], True)


    # Settings
    def check_settings_empty(self):
        r = requests.get(base_url + '/settings', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        self.assertEqual(r.json(), {'_id': r.json()['_id']})


    def check_settings_update(self):
        s1 = {
            u'site_url': 'abc',
            u'facebook_app_id': 'abc',
            u'facebook_app_secret': 'abc',
            u'twitter_app_id': 'abc',
            u'twitter_app_secret': 'abc',
            u'smtp_from': 'abc',
            u'smtp_host': 'abc',
            u'smtp_port': 'abc',
            u'smtp_user': 'abc',
            u'smtp_password': 'abc',
            u'smtp_ssl': 'abc'
        }
        r = requests.put(base_url + '/settings', json=s1, auth=JwtAuth(self.access_token))
        r.raise_for_status()
        r = requests.get(base_url + '/settings', auth=JwtAuth(self.access_token))
        r.raise_for_status()
        s2 = r.json()
        s1['_id'] = s2['_id']
        self.assertEqual(s1, s2)


    #####################
    ## Users
    #####################
    def test_users(self):
        from app.models import User
        EMAIL = 'abc@def.com'
        PWD = 'pwd'
        self.assertIsNone(User.get_by_email(EMAIL))
        User.create({'email': EMAIL, 'password': PWD})
        u = User.get_by_email(EMAIL)
        self.assertIsNotNone(u)
        self.assertEqual(u['email'], EMAIL)
        self.assertEqual(u['password'], PWD)


class JwtAuth(requests.auth.AuthBase):

    def __init__(self, access_token):
        self.access_token = access_token

    def __call__(self, r):
        r.headers['Authorization'] = 'JWT ' + self.access_token
        return r


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('MyTest').setLevel(logging.DEBUG)
    unittest.main()
