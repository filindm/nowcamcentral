# coding=UTF-8
from flask import jsonify, request, abort, g
from app import app, db
from models import User
from auth import jwt_required


@app.route('/users/self')
@jwt_required()
def get_self():
    return jsonify(g.user)


@app.route('/users', methods=['GET'])
@jwt_required()
def list_users():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    return jsonify({
        'items': User.get_all(skip, limit),
        'total': User.count()
    })


@app.route('/users/<id>', methods=['GET'])
@jwt_required()
def get_user(id):
    # print 'get_user'
    u = User.get(id)
    return jsonify(u)


@app.route('/users/rfid/<rfid>', methods=['GET'])
@jwt_required()
def get_user_by_rfid(rfid):
    # print 'get_user_by_rfid'
    u = User.find({'details.rfids': rfid}, skip=0, limit=1)
    if len(u) == 0:
        abort(404)
    return jsonify(u[0])


@app.route('/users', methods=['POST'])
@jwt_required()
def create_user():
    data = request.get_json()
    if 'fb_id' in data['details']:
        abort(400, 'Cannot create user with fb_id')
    u = User.create(request.get_json())
    return jsonify(u)


@app.route('/users', methods=['PUT'])
@jwt_required()
def update_user():
    u = User.update(request.get_json())
    return jsonify(u)

