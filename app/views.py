# coding=UTF-8

import os

from flask import request, jsonify, make_response, send_file, abort, render_template, Response
from werkzeug.exceptions import BadRequest
from app import app, db, fs
from auth import jwt_required
from .models import Customer, Event, EventUserData, Unit, Photo, Settings
from bson.objectid import ObjectId
import smtplib
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import csv
import requests
import subprocess
import shutil
import uuid
import urllib
import zipfile
from io import BytesIO
import arrow
import logging


logger = logging.getLogger(__name__)


################################################################################

@app.route('/')
def index():
    return app.send_static_file('index.html')


################################################################################
## Customers
################################################################################
@app.route('/customers', methods=['GET'])
@jwt_required()
def list_customers():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    name = request.args.get('name')
    events_count = {}
    return jsonify({
        'items': Customer.get_all(skip, limit, name),
        'total': Customer.count(),
        'events_count': events_count
    })


@app.route('/customers/<id>', methods=['GET'])
@jwt_required()
def get_customer(id):
    logger.debug('get_customer, id = ' + id)
    c = Customer.get(id)
    return jsonify(c)


@app.route('/customers/<id>/logo', methods=['GET'])
@jwt_required()
def get_customer_logo(id):
    logger.debug('get_customer_logo, id = ' + id)
    logo = Customer.get_logo(id)
    if not logo:
        abort(404)
    content_type = 'image/jpeg'
    return send_file(logo, content_type)


@app.route('/customers/<id>/logo', methods=['POST'])
@jwt_required()
def set_customer_logo(id):
    logger.debug('set_customer_logo, id = ' + id)
    logger.debug('request.files contains logo: ' + str('logo' in request.files))
    logo = request.files['logo']
    Customer.set_logo(id, logo)
    return jsonify({
        'result': 'ok'
    })


@app.route('/customers', methods=['POST'])
@jwt_required()
# @use_args(Customer.webargs, locations=('form','files', 'json'))
def create_customer():
    c = Customer.create(request.get_json())
    return jsonify(c)


@app.route('/customers', methods=['PUT'])
@jwt_required()
# @use_args(Customer.webargs_with_id, locations=('form','files', 'json'))
def update_customer():
    c = Customer.update(request.get_json())
    return jsonify(c)


################################################################################
## Units
################################################################################
@app.route('/units', methods=['GET'])
@jwt_required()
def list_units():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    return jsonify({
        'items': Unit.get_all(skip=skip, limit=limit),
        'total': Unit.count()
    })


@app.route('/units/<id>', methods=['GET'])
@jwt_required()
def get_unit(id):
    u = Unit.get(id)
    if u is None: abort(404)
    if 'event' in u:
        del u['event']
    return jsonify(u)


@app.route('/units', methods=['POST'])
@jwt_required()
def create_unit():
    u = Unit.create(request.get_json())
    return jsonify(u)


@app.route('/units', methods=['PUT'])
@jwt_required()
def update_unit():
    u = Unit.update(request.get_json())
    return jsonify(u)


################################################################################
## Photos
################################################################################
@app.route('/photos', methods=['GET'])
@jwt_required() 
def list_photos():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    event_id = request.args['event_id']
    code = request.args.get('code', None)
    photos = Photo.get_all(event_id, skip=skip, limit=limit, code=code, show_hidden=True)
    return jsonify({
        'items': photos,
        'total': Photo.count(event_id, count_hidden=True)
    })


@app.route('/photos/public', methods=['GET'])
# @jwt_required() 
def list_photos_public():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    event_id = request.args['event_id']
    code = request.args.get('code', '').upper()
    photos = Photo.get_all(event_id, skip=skip, limit=limit, code=code, show_hidden=False)
    return jsonify({
        'items': photos,
        'total': Photo.count(event_id, code=code, count_hidden=False)
    })


@app.route('/photos/<id>/image', methods=['GET'])
# @jwt_required()
def get_photo_image(id):
    img, mime_type = Photo.get_image(id)
    if img is None:
        abort(404)
    return send_file(img, mime_type)


@app.route('/photos/<id>/image.jpeg', methods=['GET'])
# @jwt_required()
def get_photo_image_jpeg(id):
    img, mime_type = Photo.get_image(id)
    if img is None:
        abort(404)
    return send_file(img, mime_type)


@app.route('/photos/<id>/image.gif', methods=['GET'])
# @jwt_required()
def get_photo_image_gif(id):
    img, mime_type = Photo.get_image(id)
    if img is None:
        abort(404)
    return send_file(img, mime_type)


@app.route('/photos/download', methods=['GET'])
# @jwt_required()
def download_photo():
    event_id = request.args['event_id']
    code = request.args['code']
    photos = Photo.get_all(event_id=event_id, code=code)
    buf = BytesIO()
    with zipfile.ZipFile(buf, 'w') as zf:
        for p in photos:
            data = zipfile.ZipInfo(p['name'])
            data.compress_type = zipfile.ZIP_DEFLATED
            img,_ = Photo.get_image(p['_id'])
            zf.writestr(data, img.read())
    buf.seek(0)
    for p in photos:
        Photo.update_stat(p['uuid'], 'downloaded')
    r = send_file(buf, attachment_filename='{}.zip'.format(code), as_attachment=False)
    return r


@app.route('/photos/<id>/download_image', methods=['GET'])
# @jwt_required()
def download_photo_image(id):
    r = get_photo_image(id)
    p = Photo.get(id)
    update_stat('downloaded', p['uuid'])
    return r


@app.route('/photos/<id>/download_video', methods=['GET'])
# @jwt_required()
def download_photo_video(id):
    r = get_photo_video(id)
    p = Photo.get(id)
    update_stat('downloaded', p['uuid'])
    return r


@app.route('/photos/<id>/thumbnail', methods=['GET'])
# @jwt_required()
def get_photo_thumbnail(id):
    img, mime_type = Photo.get_thumbnail(id)
    if img is None:
        abort(404)
    return send_file(img, mime_type)


@app.route('/photos/<id>/video.mp4', methods=['GET'])
# @jwt_required()
def get_photo_video(id):
    video, mime_type = Photo.get_video(id)
    if video is None:
        abort(404)
    return send_file(video, mime_type)


# @app.route('/photos/<id>/video', methods=['POST'])
# # @jwt_required()
# def set_photo_video(id):
#     Photo.set_image(id, request.files['file'])
#     return 'ok'


@app.route('/photos', methods=['POST'])
# @jwt_required()
def create_photo():
    args = request.args.copy()
    args.update(request.form)
    e = Event.get(args['event'])
    if e is None:
        abort(404)
    p = Photo.create(args)
    Photo.set_image(p['_id'], request.files['file'])
    try:
        add_photo_to_fb_album(e['_id'], p['_id'])
    except Exception as ex:
        logger.error('Failed to post photo to facebook: ' + str(ex))
    try:
        img, mime_type = Photo.get_image(p['_id'])
        if mime_type == 'image/gif':
            logger.debug('creating mp4 from gif')
            base_path = '/tmp/' + str(uuid.uuid4())
            gif_path = base_path + '.gif'
            mp4_path = base_path + '.mp4'
            with open(gif_path, 'wb') as f:
                shutil.copyfileobj(img, f)
            logger.debug('creating mp4 from gif: before ffmpeg')
            subprocess.call([
                'ffmpeg', '-i', gif_path,
                '-movflags', 'faststart',
                '-pix_fmt', 'yuv420p',
                '-vf', 'scale=trunc(iw/2)*2:trunc(ih/2)*2',
                mp4_path
            ])
            logger.debug('creating mp4 from gif: after ffmpeg')
            with open(mp4_path, 'rb') as f:
                Photo.set_video(p['_id'], f)
            os.unlink(gif_path)
            os.unlink(mp4_path)
    except Exception as ex:
        logger.error('Failed to create mp4: ' + str(ex))
    return jsonify(p)


@app.route('/photos/<id>/email', methods=['POST'])
# @jwt_required()
def email_photo(id):

    to_addrs = request.json['emails']

    settings = Settings.get()
    from_addr = settings['smtp_from']
    host = settings['smtp_host']
    port = int(settings['smtp_port'])
    user = settings['smtp_user']
    pwd = str(settings['smtp_password'])
    ssl = settings['smtp_ssl']

    logger.debug('to_addrs: ' + str(to_addrs))
    logger.debug('host: ' + str(host))
    logger.debug('port: ' + str(port))
    logger.debug('user: ' + str(user))
    logger.debug('pwd: ' + str(pwd))
    logger.debug('ssl: ' + str(ssl))

    # from_addr = 'andrew@nowmarketing.com.au'
    # host = 'mail.smtp2go.com'
    # port = 465
    # user = 'nowcamproject'
    # pwd  = 'M!nimarket123'
    # ssl  = True

    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = ','.join(to_addrs)
    # msg.preamble = 'Photo Description'

    p = Photo.get(id)
    if p is None:
        abort(404)
    img, mime_type = Photo.get_image(id)
    if img is None:
        abort(404)
    if mime_type == 'image/jpeg':
        file_ext = '.jpg'
    elif mime_type == 'image/gif':
        # file_ext = '.gif'
        img, mime_type = Photo.get_video(id)
        file_ext = '.mp4'
    else:
        abort(500)
    img = MIMEImage(img.read(), mime_type)
    img.add_header('Content-Disposition', 'inline', filename=p['code'] + file_ext)
    msg.attach(img)

    e = Event.get(p['event'])
    if e is None:
        abort(404)
    msg['Subject'] = e.get('microsite_sharing_email_subject', '')
    msg_body = e.get('microsite_sharing_email_body', '')
    msg_body = MIMEText(unicode(msg_body), 'html', 'utf-8')
    msg.attach(msg_body)

    if ssl:
        if port:
            server = smtplib.SMTP_SSL(host, port)
        else:
            server = smtplib.SMTP_SSL(host)
    else:
        if port:
            server = smtplib.SMTP(host, port)
        else:
            server = smtplib.SMTP(host)
    server.set_debuglevel(1)
    # logger.debug('user: ' + user)
    # logger.debug('pwd: ' + pwd)
    server.login(user, pwd)
    server.sendmail(from_addr, to_addrs, msg.as_string())
    server.quit()

    update_stat('email', p['uuid'])

    return jsonify({'ok': True})


# @app.route('/photos/<photo_id>/fb', methods=['POST'])
def add_photo_to_fb_album(event_id, photo_id):
    logger.debug('add_photo_to_fb_album')
    e = Event.get(event_id)
    if e is None:
        abort(404)
    album_id = e['fb_album']
    access_token = e['fb_access_token']
    p = Photo.get(photo_id)
    if p is None or 'image' not in p or p['image'] is None:
        abort(404)
    settings = Settings.get()
    site_url = settings['site_url']
    data = urllib.urlencode({
        'access_token': access_token,
        'no_story': 'true'
    })
    photo_url = site_url + '/photos/' + str(photo_id) + '/image'
    cmd = [
        'curl', '-s', '-o', '/dev/null', '-w', '"%{http_code}"', '-X', 'POST', 
        'https://graph.facebook.com/{}/photos?'.format(album_id) + data,
        '-d', 'url={}'.format(photo_url),
    ]
    logger.debug('cmd: ' + ' '.join(cmd))
    status_code = subprocess.check_output(cmd)
    status_code = status_code.replace('"', '')
    logger.debug('status_code: ' + str(status_code))
    if status_code != '200':
        raise Exception('Request failed: ' + str(status_code))
    # update_stat('facebook', photo_id)
    return jsonify({'ok': True})


@app.route('/photos', methods=['PUT'])
@jwt_required()
def update_photos():
    photos = request.get_json()['items']
    Photo.bulk_update(photos)
    return jsonify({'ok': True})


################################################################################
## Microsite
################################################################################
@app.route('/microsite')
def microsite():
    return app.send_static_file('microsite.html')


################################################################################
## Twitter
################################################################################
from .oauth import get_twitter_oauth
from flask import url_for
from flask import redirect
from twitter import Api as TwitterApi


@app.route('/login_twitter')
def login_twitter():
    photo_id = request.args.get('photo_id')
    twitter = get_twitter_oauth()
    return twitter.authorize(callback=url_for('oauth_authorized', photo_id=photo_id), force_login=True)


@app.route('/oauth-authorized')
def oauth_authorized():
    photo_id = request.args.get('photo_id')
    twitter = get_twitter_oauth()
    resp = twitter.authorized_response()
    if resp is None:
        return 'Oops'
    settings = Settings.get()
    api = TwitterApi(consumer_key=settings['twitter_app_id'],
                    consumer_secret=settings['twitter_app_secret'],
                    access_token_key=resp['oauth_token'],
                    access_token_secret=resp['oauth_token_secret'])

    img_url = url_for('get_photo_image_gif', id=photo_id, _external=True)
    # img_url = url_for('get_photo_video', id=photo_id, _external=True)
    p = Photo.get(photo_id)
    if p is None or 'event' not in p or p['event'] is None:
        abort(404)
    e = Event.get(p['event'])
    text = e.get('microsite_sharing_twitter_text', '')
    api.PostMedia(status=text, media=img_url)
    # api.PostUpdate(status=text, media=img_url)

    # img, mime_type = Photo.get_image(photo_id)
    # if img is None:
    #     abort(404)
    # if mime_type == 'image/gif':
    #     # img, mime_type = Photo.get_video(photo_id)
    #     img_url = url_for('get_photo_video', id=photo_id, _external=True)
    # else:
    #     img_url = url_for('get_photo_image_jpeg', id=photo_id, _external=True)
    # p = Photo.get(photo_id)
    # if p is None or 'event' not in p or p['event'] is None:
    #     abort(404)
    # e = Event.get(p['event'])
    # text = e.get('microsite_sharing_twitter_text', '')
    # api.PostUpdate(status=text, media=img_url)    

    # img, mime_type = Photo.get_image(photo_id)
    # if mime_type == 'image/gif':
    #     from PIL import Image, ImageSequence
    #     img = Image.open(fp=img)
    #     img = ImageSequence.Iterator(img)[0]
    #     from io import BytesIO
    #     buf = BytesIO()
    #     img.save(buf, 'GIF')
    #     buf.seek(0)
    #     img = buf
    # p = Photo.get(photo_id)
    # if p is None or 'event' not in p or p['event'] is None:
    #     abort(404)
    # e = Event.get(p['event'])
    # text = e.get('microsite_sharing_twitter_text', '')
    # api.PostMedia(status=text, media=img)    

    update_stat('twitter', p['uuid'])
    return redirect(url_for('twitter_oauth_done'))


@app.route('/twitter-oauth-done')
def twitter_oauth_done():
    return """
    <script>window.close()</script>
    """


################################################################################
## Facebook
################################################################################
from .oauth import get_facebook_oauth

@app.route('/login_facebook')
def login_facebook():
    photo_id = request.args.get('photo_id')
    facebook = get_facebook_oauth()
    settings = Settings.get()
    return facebook.authorize(callback=settings['site_url'] + '/oauth-fb-authorized?photo_id=' + photo_id)


@app.route('/oauth-fb-authorized')
def oauth_fb_authorized():
    # logger.debug('oauth_fb_authorized')
    photo_id = request.args.get('photo_id')
    facebook = get_facebook_oauth()
    resp = facebook.authorized_response()
    if resp is None:
        logger.debug('resp is None')
        return 'Oops'
    access_token = resp['access_token']
    settings = Settings.get()
    data = {
        'link': settings['site_url'] + '/photos/' + photo_id + '/image',
        'access_token': access_token
    }
    data = urllib.urlencode(data)
    status_code = subprocess.check_output([
        'curl', '-s', '-o', '/dev/null', '-w', '"%{http_code}"', '-X', 'POST', 
        'https://graph.facebook.com/me/feed?' + data
    ])
    status_code = status_code.replace('"', '')
    logger.debug('status_code: ' + str(status_code))
    if status_code != '200':
        raise Exception('Request failed: ' + str(status_code))
    update_stat('facebook', Photo.get(id)['uuid'])
    return redirect(settings['site_url'] + '/facebook-oauth-done')


@app.route('/facebook-oauth-done')
def facebook_oauth_done():
    return """
    <script>window.close()</script>
    """


################################################################################
## Settings
################################################################################
@app.route('/settings', methods=['GET'])
@jwt_required()
def get_settings():
    return jsonify(Settings.get())


@app.route('/settings', methods=['POST', 'PUT'])
@jwt_required()
def update_settings():
    r = Settings.update(request.get_json())
    return jsonify(r)


################################################################################
## RFID
################################################################################

@app.route('/rfid/user_data', methods=['POST'])
# @jwt_required()
def post_rfid_user_data():
    user_data = request.get_json()
    print 'user_data: ' + str(user_data)
    db.rfid_user_data.insert_one(user_data)
    return 'ok'


################################################################################
## Error handlers
################################################################################

@app.errorhandler(400)
def error_bad_request(err):
    return make_response(jsonify({
        'error': 'Bad request',
        'description': err.description
    }), 400)


@app.errorhandler(401)
def error_bad_request(err):
    return make_response(jsonify({
        'error': 'Authorization required'
    }), 401)


@app.errorhandler(404)
def error_not_found(err):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(405)
def error_method_not_allowed(err):
    return make_response(jsonify({
        'error': 'Method not allowed',
        'description': err.description
    }), 405)

###############################################################################
@app.errorhandler(Exception)
def error_server_error(err):
    logger.exception('Error')
    return make_response(jsonify({
        'error': 'Server error',
        'description': str(err)
    }), 500)



