# coding=UTF-8
from flask import jsonify, request, abort, send_file, g
from app import app, db
from models import Event, EventUserData, Photo
from auth import jwt_required
import arrow
import logging


logger = logging.getLogger(__name__)


################################################################################
## Events
################################################################################
@app.route('/events', methods=['GET'])
@jwt_required()
def list_events():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 100))
    name = request.args.get('name')
    roles = g.user['roles'] or []
    only_active = not 'admin' in roles and not 'customer' in roles
    if 'customer' in roles:
        customer = g.user['details']['customer']
    else:
        customer = None
    return jsonify({
        'items': Event.get_all(skip, limit, name, only_active, customer),
        'total': Event.count()
    })


@app.route('/events/<id>', methods=['GET'])
@jwt_required()
def get_event(id):
    e = Event.get(id)
    if e is None:
        abort(404)
    return jsonify(e)


@app.route('/events/<id>/public', methods=['GET'])
def get_event_public(id):
    e = Event.get(id)
    if e is None:
        abort(404)
    fields_to_return = [
        '_id',
        'name',
        'facebook_app_id',
        'twitter_app_id',
        'microsite_fb_app_id',
        'microsite_css',
        'microsite_subheader',
        'microsite_data_fields',
        'microsite_data_text',
        'microsite_sharing_facebook',
        'microsite_sharing_twitter',
        'microsite_sharing_instagram',
        'microsite_sharing_emails',
        'microsite_sharing_text',
        'microsite_location_and_date',
        'microsite_privacy_policy',
    ]
    return jsonify({key: e.get(key) for key in fields_to_return})


@app.route('/events', methods=['POST'])
@jwt_required()
def create_event():
    e = Event.create(request.get_json())
    return jsonify(e)


@app.route('/events', methods=['PUT'])
@jwt_required()
def update_event():
    data = request.get_json()
    if 'microsite_data_fields' in data:
        for x in data['microsite_data_fields']:
            if '$$hashKey' in x:
                del x['$$hashKey']
    e = Event.update(data)
    return jsonify(e)


@app.route('/events/<id>/microsite_logo', methods=['POST'])
@jwt_required()
def set_microsite_logo(id):
    logo = request.files.get('microsite_logo', None)
    Event.set_microsite_logo(id, logo)
    return jsonify({
        'result': 'ok'
    })


@app.route('/events/<id>/microsite_logo', methods=['GET'])
# @jwt_required()
def get_microsite_logo(id):
    logo = Event.get_microsite_logo(id)
    if not logo:
        abort(404)
    content_type = 'image/jpeg'
    return send_file(logo, mimetype=content_type, add_etags=False)


@app.route('/events/<id>/microsite_bkg', methods=['POST'])
@jwt_required()
def set_microsite_bkg(id):
    bkg = request.files.get('microsite_bkg', None)
    Event.set_microsite_bkg(id, bkg)
    return jsonify({
        'result': 'ok'
    })


@app.route('/events/<id>/microsite_bkg', methods=['GET'])
# @jwt_required()
def get_microsite_bkg(id):
    bkg = Event.get_microsite_bkg(id)
    if not bkg:
        abort(404)
    content_type = 'image/jpeg'
    return send_file(bkg, mimetype=content_type, add_etags=False)


@app.route('/events/<id>/ticket_body', methods=['POST'])
@jwt_required()
def set_ticket_body(id):
    ticket_body = request.files.get('ticket_body', None)
    Event.set_ticket_body(id, ticket_body)
    return jsonify({
        'result': 'ok'
    })


@app.route('/events/<id>/ticket_body', methods=['GET'])
# @jwt_required()
def get_ticket_body(id):
    ticket_body = Event.get_ticket_body(id)
    if not ticket_body:
        abort(404)
    content_type = 'image/jpeg'
    return send_file(ticket_body, mimetype=content_type, add_etags=False)


@app.route('/events/<id>/overlay', methods=['POST'])
@jwt_required()
def set_overlay(id):
    overlay = request.files.get('overlay', None)
    Event.set_overlay(id, overlay)
    return jsonify({
        'result': 'ok'
    })


@app.route('/events/<id>/overlay', methods=['GET'])
# @jwt_required()
def get_overlay(id):
    overlay = Event.get_overlay(id)
    if not overlay:
        abort(404)
    content_type = 'image/jpeg'
    return send_file(overlay, mimetype=content_type, add_etags=False)


################################################################################
## Event User Data
################################################################################
@app.route('/events/<id>/user_data', methods=['POST'])
# @jwt_required()
def post_user_data(id):
    if Event.get(id) is None:
        abort(404)
    user_data = request.get_json()
    EventUserData.create({
        'event': id,
        'user_data': user_data
    })
    return jsonify({
        'result': 'ok'
    })


@app.route('/events/<id>/user_data', methods=['GET'])
@jwt_required()
def get_user_data(id):
    e = Event.get(id)
    if e is None: abort(404)
    microsite_data_fields = e.get('microsite_data_fields')
    if microsite_data_fields is not None:
        user_data = EventUserData.find({'event': id})
        fieldnames = [x['name'] for x in microsite_data_fields]
        def generate():
            yield ','.join(['date'] + fieldnames) + '\n'
            for r in user_data:
                dob = arrow.get(r['_id'].generation_time).to('Australia/Sydney').format('DD/MM/YYYY HH:mm:ss')
                yield ','.join([unicode(dob)] + map(lambda x: u'"' + unicode(r['user_data'].get(x, '')) + u'"', fieldnames)) + '\n'
        res = Response(generate(), mimetype='text/csv')
        res.headers['Content-Disposition'] = 'attachment; filename=data.csv'
        return res
    return 'No data fields'


################################################################################
## Event Statistics
################################################################################
@app.route('/events/<photo_id>/stat/<channel>', methods=['POST'])
# @jwt_required()
def update_stat(channel, photo_uuid):
    Photo.update_stat(photo_uuid, channel)


@app.route('/events/<id>/stat', methods=['GET'])
@jwt_required()
def get_event_stat(id):
    return jsonify(Photo.get_stat(id))

