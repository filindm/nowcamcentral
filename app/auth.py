# coding=UTF-8
from datetime import datetime, timedelta
from functools import wraps
from flask import jsonify, request, abort, g
from app import app, db
from models import User
import jwt
from werkzeug.security import safe_str_cmp
import requests
import logging


logger = logging.getLogger(__name__)

JWT_ALGORITHM = 'HS256'


def create_token(user):
    payload = {
        'user_id': str(user['_id']),
        'exp': datetime.utcnow() + app.config['JWT_EXPIRATION_DELTA']
    }
    return jwt.encode(payload, app.config['SECRET_KEY'], JWT_ALGORITHM)


def get_or_create_user_with_fb_access_token(fb_access_token):
    url = 'https://graph.facebook.com/v2.5/me?fields=id,first_name,last_name,birthday,email&access_token=' + fb_access_token
    r = requests.get(url)
    r.raise_for_status()
    data = r.json()
    user = User.get_by_fb_id(data['id'])
    if not user:
        user = User.create({
            'email': data.get('email', ''),
            'password': '',
            'active': True,
            'confirmed_at': datetime.utcnow(),
            'roles': [],
            'details': {
                'fb_id': data['id'],
                'first_name': data.get('first_name', ''),
                'last_name': data.get('last_name', ''),
                'fb_access_token': fb_access_token
            }
        })
    return user


@app.route('/auth', methods=['POST'])
def login():
    data = request.get_json()
    if 'fb_access_token' in data:
        fb_access_token = data.get('fb_access_token')
        if not fb_access_token:
            abort(401)
        else:
            user = get_or_create_user_with_fb_access_token(fb_access_token)
            if not user:
                logger.error('Failed to get user from facebook')
                abort(500)
            jwt_token = create_token(user)
            return jsonify({'access_token': jwt_token.decode('utf-8')})
    else:
        email = data.get('username')
        password = data.get('password')
        if not email or not password:
            abort(401)
        user = User.get_by_email(email)
        if not user:
            abort(401)
        if 'details' in user and 'fb_id' in user['details']:
            logger.warn('Attempt to login a Facebook user via email/password')
            abort(401, 'Attempt to login a Facebook user via email/password')
        if safe_str_cmp(user['password'].encode('utf-8'), password.encode('utf-8')):
            jwt_token = create_token(user)
            return jsonify({'access_token': jwt_token.decode('utf-8')})
        else:
            abort(401)


def jwt_required():
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            jwt_token = request.headers.get('Authorization')
            if not jwt_token:
                abort(401)
            if not jwt_token.startswith('JWT '):
                logger.debug('JWT Authorization header must start with JWT ')
                abort(401)
            jwt_token = jwt_token[4:]
            try:
                payload = jwt.decode(jwt_token, app.config['SECRET_KEY'], algorithms=[JWT_ALGORITHM])
            except:
                abort(401)
            try:
                user = User.get(payload['user_id'])
            except:
                user = User.get(payload['identity']) # for backward compatibility
            if not user:
                abort(401)
            g.user = user
            return func(*args, **kwargs)
        return wrapper
    return decorator
