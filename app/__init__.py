# coding=UTF-8

import os
import logging, logging.handlers, logging.config
from flask import Flask
from pymongo import MongoClient
import gridfs
from flask.json import JSONEncoder
from bson.objectid import ObjectId
from datetime import datetime
import arrow

################################################################################
app = Flask(__name__)
app.config.from_object('config')
if os.environ.get('NOWCAM_SETTINGS'):
    app.config.from_envvar('NOWCAM_SETTINGS')
app.config.update({x[len("NOWCAM_"):]:os.environ[x] for x in os.environ if x.startswith("NOWCAM_")})


################################################################################
logging.config.dictConfig(app.config['LOGGING'])
logger = logging.getLogger()
logger.debug('########### DEBUG ################')


################################################################################
## DB
################################################################################
db = None
fs = None

# @app.before_first_request
def init_db():
    global db, fs
    db = MongoClient(host=app.config['MONGO_URL'], connect=False)[app.config['MONGO_DBNAME']]
    fs = gridfs.GridFS(db)
    import auth
    import users
    import events


################################################################################
class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        if isinstance(obj, datetime):
            return str(arrow.get(obj))
        return JSONEncoder.default(self, obj)

app.json_encoder = CustomJSONEncoder


# ################################################################################
# # Initial DB seed
# @app.before_first_request
# def seed_db():
#     recreate_db = False
#     if recreate_db:
#         models.Customer.objects.delete()
#         models.Event.objects.delete()
#         models.User.objects.delete()
#         models.Unit.objects.delete()
#         models.Customer(name='Customer One').save()
#         models.Customer(name='Customer Two').save()
#         models.Customer(name='Customer Three').save()
#         models.Event(name='Event One', customer=models.Customer.objects[0]).save()
#         models.Event(name='Event Two', customer=models.Customer.objects[0]).save()
#         models.Event(name='Event Three', customer=models.Customer.objects[1]).save()
#         models.User(email='filindm@gmail.com', password='password').save()
#         models.Unit(name='Unit One', mac='00:17:c4:9a:ff:15', event=models.Event.objects[0]).save()
