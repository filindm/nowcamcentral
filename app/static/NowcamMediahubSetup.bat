@echo off

net file 1>nul 2>nul
if errorlevel 1 (echo ERROR! This script must be run as Administrator & pause & exit 1)

@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
if errorlevel 1 (echo ERROR! Installation of cholatey failed & pause & exit 1)

choco install python2 --version 2.7.11 -y --force
choco install mongodb --version 3.2.9 -y --force
choco install nssm --version 2.24.0 -y --force
choco install imagemagick -y --ignore-checksums --force
choco install git -y --force
rem refreshenv

mkdir c:\mongodb\data\db
mkdir c:\mongodb\data\log
echo systemLog: > c:\mongodb\mongod.cfg
echo     destination: file >> c:\mongodb\mongod.cfg
echo     path: c:\mongodb\data\log\mongod.log >> c:\mongodb\mongod.cfg
echo storage: >> c:\mongodb\mongod.cfg
echo     dbPath: c:\mongodb\data\db >> c:\mongodb\mongod.cfg
sc.exe create MongoDB binPath= "c:\program files\mongodb\server\3.2\bin\mongod.exe --service --config=\"c:\mongodb\mongod.cfg\"" DisplayName= "MongoDB" start= "auto"
net start MongoDB

mkdir "%HOMEPATH%\.ssh"
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBZ3Frvftqq6JsK1l7xvruao677UAfUNE6jxibG4w0+zWesQkIfQTQUIzDZClq5/xMEq0lP6gaxPp6NNC/FPrVBXpO1mU0nFbaKKVoqp5zj8BiFuCk99DQBMqjww7S6RrjCgXD2jaFxdHPlUhB93NfzfXdQyi6vwBKjgstX81CZO85b9uSsQ6xyaJFwYvJPBawjiv1fBuh+RzlSKuR+avrawjd8jygyfsu3fBa+F+xGyJKTTQT9toawWgQ3x0dWB/c0rQ6HUri0UWR7g7ZcNCVkhm+OUelPB2Ks+8vczQBH7mNWHFeiN5gO59qsKxYHaiE4/ok+2URzxKX2+U18K3l fdm@fdm-win7 > "%HOMEPATH%\.ssh\id_rsa.pub"

echo -----BEGIN RSA PRIVATE KEY----- > "%HOMEPATH%\.ssh\id_rsa"
echo MIIEpAIBAAKCAQEAwWdxa737aquibCtZe8b67mqOu+1AH1DROo8YmxuMNPs1nrEJ >> "%HOMEPATH%\.ssh\id_rsa"
echo CH0E0FCMw2Qpauf8TBKtJT+oGsT6ejTQvxT61QV6TtZlNJxW2iilaKqec4/AYhbg >> "%HOMEPATH%\.ssh\id_rsa"
echo pPfQ0ATKo8MO0uka4woFw9o2hcXRz5VIQfdzX8313UMour8ASo4LLV/NQmTvOW/b >> "%HOMEPATH%\.ssh\id_rsa"
echo krEOscmiRcGLyTwWsI4r9Xwbofkc5Uirkfmr62sI3fI8oMn7Lt3wWvhfsRsiSk00 >> "%HOMEPATH%\.ssh\id_rsa"
echo E/baGsFoEN8dHVgf3NK0Oh1K4tFFke4O2XDQlZIZvjlHpTwdirPvL3M0AR+5jVhx >> "%HOMEPATH%\.ssh\id_rsa"
echo XojeYDufarCsWB2ohOP6JPtlEc8Sl9vlNfCt5QIDAQABAoIBACMV0fSavW5nfoqi >> "%HOMEPATH%\.ssh\id_rsa"
echo /pO+0CzKNeSPsK89qQc3iF4zDCdcXyjPsvTQQTxNMIwNspnLRfDC/pggSS26CDp9 >> "%HOMEPATH%\.ssh\id_rsa"
echo NFVFyA829C4zJJ77saHX99HaCe6q6FqjDdO/hSkHhX9Z1mmakh8X5V3XS0bPUTWY >> "%HOMEPATH%\.ssh\id_rsa"
echo PkiBTHPX2D9wfIYMOlBMHmj31TOy7PUGPD7yCMkLGLWMTN/WclpD7ISPXFyCTqj/ >> "%HOMEPATH%\.ssh\id_rsa"
echo fnoY5/EZuR4Zlu9Ocn/+Kxkoznyjgzg8tDX2N0i1jL0lns53/rMD7WpTIfTLADLL >> "%HOMEPATH%\.ssh\id_rsa"
echo dX8DmEqaR2XMhnnUDkx48NaSxg/aLltZq+ljtPHP3l2A38wIZmMAC3WWH2f2rW26 >> "%HOMEPATH%\.ssh\id_rsa"
echo jaRueCECgYEA7IGH2YeNnYcSX3838fpoMT93er8autZPJqL8M/YXUS5SrMSwkQJo >> "%HOMEPATH%\.ssh\id_rsa"
echo /gs5jhCK4TDH5R4LuUavgdEfbXztB95+BCoaB09tz+CGYIJNITl+86ZLuBbrh0F3 >> "%HOMEPATH%\.ssh\id_rsa"
echo bdRSW+bzZv3mRkOubq0WKgAFrhYaRZ78JCU4PoYe8dpAWzcv/NkwEpkCgYEA0Vht >> "%HOMEPATH%\.ssh\id_rsa"
echo T1dhB+SLQPPzCVJianRKgQ4pB3tExuyICZYrel6pKUOjiO6Zy7ZvtqXmXR3FV7/X >> "%HOMEPATH%\.ssh\id_rsa"
echo W5vEKd16gAV6lBz2zrKZqMxxKMyn79pt3k4TgwK6YIg75Sjdjb6JbOEx0WRJqOnu >> "%HOMEPATH%\.ssh\id_rsa"
echo EbMzTkHrIou0oF4hW3mQrZxfUyc76lYZdQNTUS0CgYEAg3OzWDq5gZbzpVSGed5e >> "%HOMEPATH%\.ssh\id_rsa"
echo TxEhjX3uk25ObAEnAJKFfz+qABEKum6GgL0obkfPiT6dvEEo7v/UyOi8+RP4gukt >> "%HOMEPATH%\.ssh\id_rsa"
echo TY9BDFMfi+0Vg0qpKBBW+YGykE1c975xbQAYDrCdFN+GQAOaTjWyHgIJJituyseF >> "%HOMEPATH%\.ssh\id_rsa"
echo CWoOiuHtDSvrnUddSsJEr/ECgYBFXHkLOrZJeS2yXaF+yy8UemddDyYE9pC5IBGK >> "%HOMEPATH%\.ssh\id_rsa"
echo q5jIOKjvpRt/+R434sBs0dbbr0t9YXCnR5rdDCbUV/xfKqGTppVG2kX78KML02Fn >> "%HOMEPATH%\.ssh\id_rsa"
echo sJLCa5r49OpzaPDPT/wUeSCachH9x1Q7xRYQrcKMdaFpKQcAWezXuOHUmUE+yTwy >> "%HOMEPATH%\.ssh\id_rsa"
echo CuIGGQKBgQCwtkaK6jUwcFy2D/Tr2GZsONzWMUWwkPm19x4eACbojeKwOh8PTHH6 >> "%HOMEPATH%\.ssh\id_rsa"
echo 6dZO7OaCyLp5GsrtpKhqocS/WnxjkUfnuH2Sr+z5/5UoV7rGW8YPgjYcp+4G+0Y7 >> "%HOMEPATH%\.ssh\id_rsa"
echo xfkd38/B/RYPNhkh/AVUuwYffTWo74u8spN6wWvBa3ZI6WoAi7kJ0Q== >> "%HOMEPATH%\.ssh\id_rsa"
echo -----END RSA PRIVATE KEY----- >> "%HOMEPATH%\.ssh\id_rsa"

echo bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw== > "%HOMEPATH%\.ssh\known_hosts"

echo Installing NowcamMediaHub...
set nssm=c:\programdata\chocolatey\bin\nssm.exe

%nssm% status "NowcamMediaHub" 1>nul &&           %nssm% stop  "NowcamMediaHub"
%nssm% status "NowcamMediaHub Broadcast" 1>nul && %nssm% stop  "NowcamMediaHub Broadcast"
%nssm% status "NowcamTetheredCamera" 1>nul &&     %nssm% stop  "NowcamTetheredCamera"
%nssm% status "NowcamVideoGif" 1>nul &&           %nssm% stop  "NowcamVideoGif"

del "%TMP%\NowcamMediaHub.zip"
"C:\Program Files\Git\cmd\git" archive --format=zip --prefix=NowcamMediaHub/ --remote=git@bitbucket.org:filindm/nowcammediahub.git HEAD > "%TMP%\NowcamMediaHub.zip"
c:\tools\python2\Scripts\pip install --upgrade "%TMP%\NowcamMediaHub.zip"
del "%TMP%\NowcamMediaHub.zip"
c:\tools\python2\Scripts\pip install pypiwin32==219

mkdir c:\NowcamMediaHub
%nssm% status "NowcamMediaHub" 2>nul || %nssm% install "NowcamMediaHub" c:\tools\python2\Scripts\nowcammediahub.exe
%nssm% set "NowcamMediaHub" AppDirectory c:\NowcamMediaHub
rem TODO: Set Logon to NowCam user

%nssm% status "NowcamMediaHub Broadcast" 2>nul || %nssm% install "NowcamMediaHub Broadcast" c:\tools\python2\Scripts\nowcammediahub_broadcast.exe
%nssm% set "NowcamMediaHub Broadcast" AppDirectory c:\NowcamMediaHub

mkdir c:\NowcamTetheredCamera\img_processing
%nssm% status "NowcamTetheredCamera" 2>nul || %nssm% install "NowcamTetheredCamera" c:\tools\python2\Scripts\nowcammediahub_tethered_camera.exe
%nssm% set "NowcamTetheredCamera" AppDirectory c:\NowcamTetheredCamera

mkdir c:\NowcamVideoGif\gif
%nssm% status "NowcamVideoGif" 2>nul || %nssm% install "NowcamVideoGif" c:\tools\python2\Scripts\nowcammediahub_video_gif.exe
%nssm% set "NowcamVideoGif" AppDirectory c:\NowcamVideoGif

%nssm% start  "NowcamMediaHub"
%nssm% start  "NowcamMediaHub Broadcast"
%nssm% start  "NowcamTetheredCamera"
%nssm% start  "NowcamVideoGif"
