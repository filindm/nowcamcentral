
angular.module('app', ['ngRoute', 'ngSanitize'])

.config(['$routeProvider', function($routeProvider){

    $routeProvider

    .when('/init/:event_id', {
        templateUrl: 'static/microsite.init.html',
        controller: 'InitCtrl'
    })
    .when('/enter-code', {
        templateUrl: 'static/microsite.enter.code.html',
        controller: 'EnterCodeCtrl'
    })
    .when('/gallery', {
        templateUrl: 'static/microsite.gallery.html',
        controller: 'GalleryCtrl'
    })
    .when('/privacy', {
        templateUrl: 'static/microsite.privacy.html',
        controller: 'PrivacyCtrl'
    })
    .when('/data-collection', {
        templateUrl: 'static/microsite.data.collection.html',
        controller: 'DataCollectionCtrl'
    })
    .when('/sharing', {
        templateUrl: 'static/microsite.sharing.html',
        controller: 'SharingCtrl'
    })
    .otherwise({
        redirectTo: '/enter-code'
    })
}])

.controller('AppCtrl', ['$scope', '$http', '$window', '$location', '$rootScope',
    function($scope, $http, $window, $location, $rootScope){

    $scope.bkg = {
        url: null
    }

    angular.element($window).bind('resize', function(){
        $scope.$apply();
    })

    angular.element($window).bind('message', function(e){
        // console.dir(e);
        e = e.originalEvent;
        if(e.data.action === 'activatePage'){
            // console.log(e.data.path);
            $location.path(e.data.path);
            $scope.$apply();
        } else if(e.data.action === 'setPhotos'){
            $rootScope.photos = e.data.photos;
            $rootScope.$apply();
        } else if(e.data.action === 'setLogo'){
            // console.log('message setLogo: url: ' + (e.data.url ? e.data.url.substr(0, 100) : ''));
            // console.log($('#logo'));
            $('#logo')[0].src = e.data.url;
        } else if(e.data.action === 'setBkg'){
            console.log('message setBkg: url: ' + (e.data.url ? e.data.url.substr(0, 100) : ''));
            // console.log($('#logo'));
            // $('#logo')[0].src = e.data.url;
            $scope.bkg.url = e.data.url;
            $scope.$apply();
        }
    })

    $scope.getStyle = function(name){
        if(!$scope.event || !$scope.event.microsite_css){
            return {};
        }
        if(isMobile()){
            return angular.extend({}, 
                $scope.event.microsite_css.common  ? $scope.event.microsite_css.common[name] : {},
                $scope.event.microsite_css.desktop ? $scope.event.microsite_css.desktop[name] : {},
                $scope.event.microsite_css.mobile  ? $scope.event.microsite_css.mobile[name] : {});
        } else {
            return angular.extend({}, 
                $scope.event.microsite_css.common ?  $scope.event.microsite_css.common[name] : {},
                $scope.event.microsite_css.mobile ?  $scope.event.microsite_css.mobile[name] : {},
                $scope.event.microsite_css.desktop ? $scope.event.microsite_css.desktop[name]: {});
        }
    }

    $scope.getBackgroundStyle = function(){
        if(!$scope.event){
            return {};
        }
        var bkgUrl = $scope.bkg.url || '/events/' + $scope.event._id + '/microsite_bkg';
        var bkg =  {
            background: 'url(' + bkgUrl + ') no-repeat center center fixed',
            '-webkit-background-size': 'cover',
            '-moz-background-size': 'cover',
            '-o-background-size': 'cover',
            'background-size': 'cover'
        }
        bkg = angular.extend(bkg, $scope.getStyle('bodyBackground'));
        // console.log('bkg: ' + JSON.stringify(bkg));
        return bkg;
    }

    $scope.goHome = function(){
        $location.path('/');
    }

    $scope.goto = function(path){
        $location.path('/' + path);
    }

    $scope.range = function(n) {
        return new Array(n);
    }

    function isMobile(){
        return angular.element($window).width() < 320;
    }

}])

.controller('InitCtrl', ['$http', '$routeParams', '$rootScope', '$location',
    function($http, $rootParams, $rootScope, $location){

    $http.get('/events/' + $rootParams.event_id + '/public', {timeout: 5000})
    .then(function(res){
        $rootScope.event = res.data;
        if($rootScope.event.microsite_css){
            if($rootScope.event.microsite_css.enter_code){
                $location.path('/enter-code');
            } else if($rootScope.event.microsite_css.gallery){
                $location.path('/gallery');
            } else {
                $location.path('/privacy'); // ???
            }
        } else {
            console.error('$rootScope.event.microsite_css not defined');
        }
    }, function(err){
    });

}])

.controller('EnterCodeCtrl', ['$scope', '$rootScope', '$location', '$http', 
    function($scope, $rootScope, $location, $http){

    var sem = semaphore(1);
    $scope.submit = function(){
        if($scope.photo_code){
            sem.take(function(){
                $http.get('/photos/public', {
                    params: {
                        limit: 100, //$scope.event.microsite_css.enter_code_no_gallery ? 100 : 1,
                        skip: 0, 
                        event_id: $scope.event._id,
                        code: $scope.photo_code
                    }
                }, {timeout: 5000})

                .then(function(res){
                    console.dir(res.data);
                    $rootScope.code = angular.copy($scope.photo_code);
                    $rootScope.photos = res.data.items;
                    $location.path(
                        ($scope.event.microsite_css && $scope.event.microsite_css.data_collection) ? 
                            '/data-collection' : '/sharing');
                })

                .finally(function(){
                    sem.leave();
                })
            })
        }
    }

}])

.controller('GalleryCtrl', 
    ['$scope', '$http', '$window', '$document', '$location', '$rootScope', '$interval',
    function($scope, $http, $window, $document, $location, $rootScope, $interval){

    $scope.photos = [];
    $scope.hidePhotoCode = true;
    $scope.hideEnterCodeOverlay = false;

    var sem = semaphore(1);
    var limit = 36;
    var skip = 0;

    angular.element($window).bind('scroll', function(){
        var scrollPos = (window.scrollY || window.pageYOffset);
        var windowHeight = angular.element($window).height();
        var docHeight = $document.height();
        var offset = docHeight - windowHeight - scrollPos;
        if(offset < 50){
            loadPhotos();
        }
    })

    $scope.$watch('event', function(){
        loadPhotos();
    })

    // TODO: doesn't work as expected; commented-out for now
    // var refreshTimer = $interval(function(){
    //     loadPhotos();
    // }, 5000);

    // $scope.$on('$destroy', function(){
    //     $interval.cancel(refreshTimer);
    // })

    $scope.gotoNext = function(photo){
        $rootScope.photos = [photo];
        $location.path(($scope.event.microsite_css && $scope.event.microsite_css.data_collection) ? 
            '/data-collection' : '/sharing');
    }

    $scope.showEnterCodeOverlay = function(){
        return $location.path() === '/enter-code-gallery' && !$scope.hideEnterCodeOverlay;
    }

    $scope.closeEnterCodeOverlay = function(){
        $scope.hideEnterCodeOverlay = true;
    }

    $scope.submit = function(){
        if($scope.photo_code){
            sem.take(function(){
                $http.get('/photos/public', {
                    params: {
                        limit: 1,
                        skip: 0, 
                        event_id: $scope.event._id,
                        code: $scope.photo_code
                    }
                }, {timeout: 5000})

                .then(function(res){
                    console.dir(res.data);
                    if(res.data.items.length){
                        var photo = res.data.items[0];
                        if(photo){
                            $scope.gotoNext(photo);
                        }
                    }
                })

                .finally(function(){
                    sem.leave();
                })
            })
        }
    }

    loadPhotos();

    function loadPhotos(){
        console.log('loadPhotos');
        if(!$scope.event){
            return;
        }
        console.log('loadPhotos, 1');
        sem.take(function(){
            $http.get('/photos/public', {
                params: {
                    limit: limit,
                    skip: skip, 
                    event_id: $scope.event._id,
                    code: $scope.photo_code
                }
            }, {timeout: 5000})

            .then(function(res){
                skip += res.data.items.length;
                Array.prototype.push.apply($scope.photos, res.data.items);
            })

            .finally(function(){
                sem.leave();
            })
        })
    }
}])

.controller('PrivacyCtrl', ['$scope', '$location', function($scope, $location){

    $scope.gotoNext = function(){
        $location.path('/sharing');
    }

}])


.controller('DataCollectionCtrl', ['$scope', '$location', '$http',
    function($scope, $location, $http){

    $scope.user_data = {};

    $scope.gotoNext = function(){
        console.dir($scope.user_data);
        $http({
            method: 'POST',
            url: 'events/' + $scope.event._id + '/user_data',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify($scope.user_data)
        }).finally(function(){
            $location.path('/sharing');
        })
    }
}])


.controller('SharingCtrl', ['$scope', '$location', '$http', '$timeout', '$window',
    function($scope, $location, $http, $timeout, $window){

    $scope.status = {}
    $scope.ui_state = {
        'action_in_progress': false
    }
    $scope.photo_group_mode = $scope.photos && $scope.photos.length > 1;
    if($scope.photos){
        $scope.photo = $scope.photos[0];
    }

    if(!$scope.photo_group_mode){
        loadFbSdk();
    }

    function loadFbSdk() {
        window.fbAsyncInit = function() {
            console.log('fb app id: ' + $scope.event.microsite_fb_app_id);
            FB.init({
                appId  : $scope.event.microsite_fb_app_id,
                xfbml  : true,
                version: 'v2.5'
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    $scope.fb_share = function(photo_id){
        console.log("fb_share, photo_id: " + photo_id);
        var height = 800;
        var width = 1200;
        var top = window.innerHeight / 2 - height / 2;
        var left = window.innerWidth / 2 - width / 2;
        var popup = window.open($window.location.origin + "/login_facebook?photo_id=" + photo_id,
            "FacebookPopupWindowName", 
            "width=" + width + ",height=" + height + 
            ",left=" + left + ",top=" + top);
        var timer = setInterval(checkChild, 500);
        function checkChild() {
            if (popup.closed) {
                clearInterval(timer);
                setTimeout(function(){
                    $location.path('/gallery');
                    $scope.$apply();
                }, 1000);
            }
        }
    }

    $scope.tw_share = function(photo_id){
        var height = 400;
        var width = 400;
        var top = window.innerHeight / 2 - height / 2;
        var left = window.innerWidth / 2 - width / 2;
        var popup = window.open($window.location.origin + "/login_twitter?photo_id=" + photo_id,
            "TwitterPopupWindowName", 
            "width=" + width + ",height=" + height + 
            ",left=" + left + ",top=" + top);
        var timer = setInterval(checkChild, 500);
        function checkChild() {
            if (popup.closed) {
                clearInterval(timer);
                // window.open('https://twitter.com/logout',
                //     "TwitterLogout",
                //     "width=939,height=394" + 
                //     ",left=" + left + ",top=" + top);
                setTimeout(function(){
                    $location.path('/gallery');
                    $scope.$apply();
                }, 1000);
            }
        }
    }

    $scope.email_share = function(photo_id){
        $scope.ui_state.action_in_progress = true;
        var emails = [];
        $('input[name=email]').each(function(){
            emails.push($(this).val());
        });
        console.log('emails: ' + emails);
        $http({
            method: 'POST',
            url: 'photos/' + photo_id + '/email',
            data: {emails: emails}
        }).then(function(res){
            if(res.status === 200 && res.data.ok){
                $scope.status.email_sent = true;
                setTimeout(function(){
                    $location.path('/gallery');
                    $scope.$apply();
                }, 1000);
            }
        }).finally(function(){
            $scope.ui_state.action_in_progress = false;
        })
    }

    $scope.show_email_btn = function(){
        return $scope.event.microsite_sharing_emails && !$scope.status.email_sent;
    }

    $scope.image_width_from_index = function(items, idx){
        var len = items.length;
        if(idx < len - len % 3){
            return 4;
        } else {
            return 12 / (len % 3);
        }
    }

}])

