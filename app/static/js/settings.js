/*
*   SettingsCtrl
*/
angular.module('app').controller('SettingsCtrl', 
    ['$scope', '$http', function($scope, $http){

    $scope.save = function(){
        $http({
            method: 'POST',
            url: '/settings',
            data: $scope.settings
        }).then(function(res){
            $scope.settings = res.data;
        }, function(err){
            console.error(err);
        });
    }

    function load(){
        $http.get('/settings').then(function(res){
            $scope.settings = res.data;
        }, function(err){
            console.error(err);
        });
    }

    load();

}]);
