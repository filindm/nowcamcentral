/*
*   EventsPhotosCtrl
*/
angular.module('app').controller('EventsPhotosCtrl', 
    ['$scope', '$stateParams', 'Event', 'Photo',
    function($scope, $stateParams, Event, Photo){

    $scope.event = {};
    $scope.photos = {};
    $scope.code = '';

    $scope.ui_state = {
        action_in_progress: false
    }

    
    // Pagination

    var photos_per_page = 12;
    var max_pages_count = 15;
    $scope.current_page = 0;
    $scope.start_page = 0;

    $scope.total_pages = function(){
        return Math.ceil($scope.photos.total / photos_per_page);
    }

    $scope.has_prev = function(){
        return $scope.start_page > 0;
    }

    $scope.has_next = function(){
        return $scope.start_page + max_pages_count < $scope.total_pages();
    }

    $scope.enum_pages = function(){
        var res = [];
        for(var i = $scope.start_page; 
            i < $scope.start_page + max_pages_count && i < $scope.total_pages(); 
            i++){
            res.push(i);
        }
        return res;
    }

    $scope.prev = function(){
        if($scope.has_prev()){
            $scope.start_page -= max_pages_count;
            $scope.current_page = $scope.start_page + max_pages_count - 1;
            $scope.load_photos();
        }
    }

    $scope.next = function(){
        if($scope.has_next()){
            $scope.start_page += max_pages_count;
            $scope.current_page = $scope.start_page;
            $scope.load_photos();
        }
    }

    $scope.set_current_page = function(n){
        $scope.current_page = n;
        $scope.load_photos();
    }

    $scope.submit = function(){
        $scope.ui_state.action_in_progress = true;
        Photo.bulkUpdate($scope.photos).finally(function(){
            $scope.ui_state.action_in_progress = false;
        });
    }

    $scope.load_photos = function(){
        $scope.ui_state.action_in_progress = true;
        var skip = $scope.current_page * photos_per_page;
        var limit = photos_per_page;
        if($scope.code){
            skip = 0;
            limit = 1;
        }
        Photo.getAll($stateParams.id, skip, limit, $scope.code).then(function(res){
            $scope.photos = res.data;
        }, function(err){
            $scope.error = err;
        }).finally(function(){
            $scope.ui_state.action_in_progress = false;
        })
    }

    Event.get($stateParams.id).then(function(res){
        $scope.event = res.data;
    }, function(err){
        $scope.error = err;
    });

    $scope.load_photos();

}]);


