/*
*   CustomersCtrl
*/
angular.module('app').controller('CustomersCtrl', 
    ['$scope', '$timeout', '$state', 'Customer', 'CustomersSearch', 
    function($scope, $timeout, $state, Customer, CustomersSearch){

    var limit = 10;

    $scope.customers = {
        items: [],
        total: 0,
        skip: 0,
        events_count: {},
    };

    $scope.search = CustomersSearch;

    $scope.$watch('search.filter', function(newValue){
        //console.log('watch filter: ' + newValue);
        // if(newValue.length > 2 || newValue.length == 0){
        //     console.log('timeout load');
        //     $timeout(load, 500);
        // }
        $timeout(load, 500);
    })

    $scope.onAddCustomerBtnClicked = function(){
        $state.go('customers_new');
    }

    var load_running = false;
    function load(){
        if(load_running) return;
        load_running = true;
        Customer.getAll($scope.skip, limit, $scope.search.filter)
        .then(function(res){
            $scope.customers.items = res.data.items;
            $scope.customers.total = res.data.total;
            $scope.customers.events_count = res.data.events_count;
        }, function(err){
            console.error(err);
        })
        .finally(function(){
            load_running = false;
        }) 
    }

    load();

}]);


