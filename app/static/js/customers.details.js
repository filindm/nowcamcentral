/*
*   CustomersDetailsCtrl
*/
angular.module('app').controller('CustomersDetailsCtrl', 
    ['$scope', '$stateParams', 'Customer', function($scope, $stateParams, Customer){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        // var customer = {
        //     id:             $scope.customer.id,
        //     name:           $scope.customer.name || '',
        //     contact_name:   $scope.customer.contact_name || '',
        //     contact_phone:  $scope.customer.contact_phone || '',
        //     contact_email:  $scope.customer.contact_email || '',
        //     file:           $scope.customer.logo ? $scope.customer.logo.file : null
        // }
        Customer.update($scope.customer).then(function(res){
            if($scope.customer.logo && $scope.customer.logo.file){
                console.log('_id: ' + res.data._id + ', file: ' + $scope.customer.logo.file);
                console.dir($scope.customer.logo.file);
                Customer.set_logo(res.data._id, $scope.customer.logo.file);
            }
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        // console.dir($scope.theForm);
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Customer.get($stateParams.id).then(function(resp){
        $scope.customer = resp.data;
    }, function(err){
        $scope.error = err;
    })


}]);


