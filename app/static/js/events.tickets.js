/*
*   EventsTicketsCtrl
*/
angular.module('app').controller('EventsTicketsCtrl', 
    ['$scope', '$http', '$stateParams', 'Event', 'Photo',
    function($scope, $http, $stateParams, Event, Photo){

    $scope.photos = [];
    $scope.photos_idx = 0;
    $scope.file_upload = {
        ticket_body: {}
    }

    $scope.ui_state = {
        action_in_progress: false
    }

    $scope.save = function(){
        $scope.ui_state.action_in_progress = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.ticket_body){
                if($scope.file_upload.ticket_body.remove){
                    Event.setTicketBody($scope.event._id, '');
                } else if($scope.file_upload.ticket_body.file){
                    console.dir($scope.file_upload.ticket_body.file);
                    Event.setTicketBody($scope.event._id, $scope.file_upload.ticket_body.file);                    
                }
            }
            $scope.ui_state.action_in_progress = false;

        }, function(err){
            $scope.ui_state.action_in_progress = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        console.log('=== cancel');
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
    }

    $scope.photos_next = function(){
        $scope.photos_idx = ($scope.photos_idx + 1) % $scope.photos.length;
    }

    $scope.getCodeInputBoxStyle = function(){
        if($scope.event){
            return {
                top: $scope.event.ticket_top + '%',
                left: $scope.event.ticket_left + '%',
                width: $scope.event.ticket_width + '%',
                height: $scope.event.ticket_height + '%'
            }
        } else {
            return {}
        }
    }

    $scope.getTicketImgSrc = function(){
        if($scope.file_upload.ticket_body && $scope.file_upload.ticket_body.url){
            return $scope.file_upload.ticket_body.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.ticket_body.remove){
            return '/events/' + $scope.event._id + '/ticket_body'
        }
        return 'static/img/avatar.png';        
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });

    Photo.getAll($stateParams.id, 0, 20).then(function(res){
        $scope.photos = res.data.items;
    }, function(err){
        $scope.error = err;
    });

}]);


