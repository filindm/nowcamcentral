/*
*   EventsTabs Directive
*/
angular.module('app').directive('eventsTabs', function(){
    return {
        templateUrl: 'static/partials/events.tabs.html'
    }
})
