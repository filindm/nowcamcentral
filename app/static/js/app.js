/*
*   Application
*/
angular.module('app', [
    'ui.router',
    'mgcrea.ngStrap',
    // 'minicolors'
    'colorpicker.module'
]);

angular.module('app').run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
});

/*
*   AppCtrl
*/
angular.module('app').controller('AppCtrl', 
    ['$scope', '$location', function($scope, $location){


}]);
