/**
* File Upload Directive
*/
angular.module('app').directive('fileUpload', function(){
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function link(scope, element, attrs, ngModel) {
            if(!ngModel) return;
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });
            read();

            function read() {
                // console.log('read');
                var file = element[0].files[0];
                if(!file) return;
                // console.dir(file);
                var reader = new FileReader();
                reader.onload = (function(f){
                    return function(e){
                        // console.dir('fileUpload: file: ' + JSON.stringify(f) + ', url: ' + e.target.result);
                        // console.dir(ngModel);
                        // console.dir(ngModel.$modelValue);
                        // console.dir(ngModel.$viewValue);
                        ngModel.$setViewValue({
                            file: f,
                            url: e.target.result,
                            remove: false
                        });
                        element[0].value = null;
                    }
                })(file);
                reader.readAsDataURL(file);
            }
        }
    };
});

angular.module('app').directive('nowcamImg', function(){
    return {
        restrict: 'A',
        scope: {
            url: '=url'
        },
        link: function link(scope, element, attrs) {

            var inputFile = document.createElement('input');
            inputFile.setAttribute('type', 'file');
            // var theImg = document.getElementById('theImg');
            var theImg = element.append('<img/>')[0];
            theImg.onclick = function(){
                inputFile.click();
            }
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var img = new Image();
            var url;
            img.onload = function(){
                URL.revokeObjectURL(url);
                canvas.width = img.width;
                canvas.height = img.height;
                // console.log('img WxH: ', img.width, img.height);
                // console.log('canvas WxH: ', canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0);
                var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                grayscale(imageData);
                ctx.putImageData(imageData, 0, 0);
                theImg.src = canvas.toDataURL();
                scope.url = theImg.src;
                scope.$apply();
            }
            inputFile.onchange = function(){
                url = URL.createObjectURL(inputFile.files[0]);
                img.src = url;
            }

            function grayscale(imageData) {
                var data = imageData.data;
                for (var i = 0; i < data.length; i += 4) {
                    var avg = (data[i] + data[i +1] + data[i +2]) / 3;
                    data[i]     = avg; // red
                    data[i + 1] = avg; // green
                    data[i + 2] = avg; // blue
                }
            }

            // img.src = url;
        }
    }
});

angular.module('app').directive('nowcamCrop', function(){
    return {
        restrict: 'A',
        scope: {
            x: '=',
            y: '=',
            w: '=',
            h: '=',
        },
        link: function link(scope, element, attrs) {
            var el = element.children('img')[0];
            if(el.width > 0){
                el.Jcrop({
                    onSelect: function(c){
                        scope.x = c.x;
                        scope.y = c.y;
                        scope.w = c.w;
                        scope.h = h;
                        scope.$apply();
                    }
                })
            }
        }
    }
});

