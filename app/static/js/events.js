/*
*   EventsCtrl
*/
angular.module('app').controller('EventsCtrl', 
    ['$scope', '$timeout', '$state', 'Event', 'EventsSearch', 
    function($scope, $timeout, $state, Event, EventsSearch){

    var limit = 100;

    $scope.events = {
        items: [],
        total: 0,
        skip: 0
    };

    $scope.search = EventsSearch;

    $scope.$watch('search.filter', function(newValue){
        //console.log('watch filter: ' + newValue);
        // if(newValue.length > 2 || newValue.length == 0){
        //     console.log('timeout load');
        //     $timeout(load, 500);
        // }
        $timeout(load, 500);
    })    

    $scope.onAddEventBtnClicked = function(){
        $state.go('events_new');
    }

    var load_running = false;
    function load(){
        if(load_running) return;
        load_running = true;
        Event.getAll($scope.skip, limit, $scope.search.filter)
        .then(function(res){
            $scope.events.items = res.data.items;
            $scope.events.total = res.data.total;
        }, function(err){
            console.error(err);
        })
        .finally(function(){
            load_running = false;
        }) 
    }

    load();

}]);


