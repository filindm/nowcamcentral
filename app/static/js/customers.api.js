/*
*   CustomersApi
*/
angular.module('app').service('Customer', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
    }
    var cfg = {
        baseUrl: '/customers',
    }

    return {

        getAll: function(skip, limit, name){
            var params = {
                    limit: limit,
                    skip: skip
            }
            if(name){
                params.name = name;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(data){
            return $http({
                method: 'POST',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        update: function(data){
            return $http({
                method: 'PUT',
                url: cfg.baseUrl,
                data: JSON.stringify(data),
            });
        },

        set_logo: function(id, file){
            var formData = new FormData();
            formData.append('logo', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + id + '/logo',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            })
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        }
    }
}])

