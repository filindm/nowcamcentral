/*
*   UnitsCtrl
*/
angular.module('app').controller('UnitsCtrl', 
    ['$scope', '$state', 'Unit', function($scope, $state, Unit){

    var limit = 10;

    $scope.units = {
        items: [],
        total: 0,
        skip: 0
    };

    $scope.onAddUnitBtnClicked = function(){
        $state.go('units_new');
    }

    Unit.getAll($scope.skip, limit).then(function(res){
        $scope.units.items = res.data.items;
        $scope.units.total = res.data.total;
    }, function(err){
        console.error(err);
    });

}]);


