/*
*   Auth
*/
angular.module('app').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('AuthInjector');
}]);

angular.module('app').factory('AuthInjector', 
    ['$location', '$q', 'AuthService', function($location, $q, AuthService){
    return {
        request: function(config) {
            var authToken = AuthService.token();
            if(authToken){
                config.headers['Authorization'] = 'JWT ' + authToken;
            }
            return config;
        },
        // response: function (response) {
        //     return response || $q.when(response);
        // },
        responseError: function (response) {
            if (response.status === 401) {
                $location.url('login');
                // //here I preserve login page 
                // someService
                //    .setRestrictedPageBeforeLogin(
                //             extractPreservedInfoAboutPage(response)
                //     )
                // $rootScope.$broadcast('error')
            }
            return $q.reject(response);
            //return response.reject();
        }
    };
}]);

angular.module('app').factory('AuthService', 
    ['$injector', '$q', function($injector, $q){
    return {
        login: function(username, password){
            var defer = $q.defer();
            // console.log('username: ' + username);
            $injector.get("$http").post('/auth', {
                'username': username,
                'password': password
            }).then(function(res){
                // console.log('Authentication successful, data: ');
                // console.dir(data);
                sessionStorage.authToken = res.data.access_token;
                sessionStorage.username = username;
                defer.resolve();
            }, function(err){
                defer.reject('Authentication failed, error: ' + err);
            });
            return defer.promise;
        },
        token: function(){
            return sessionStorage.authToken;
        }
    };
}]);

