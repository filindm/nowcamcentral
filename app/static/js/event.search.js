/*
*   EventsSearchCtrl
*/
angular.module('app').factory('EventsSearch', function(){
    return {filter: ''}
});

angular.module('app').controller('EventsSearchCtrl', 
    ['$scope', '$state', 'EventsSearch', function($scope, $state, EventsSearch){

    $scope.placeholder = 'Search for event';
    $scope.search = EventsSearch;

}]);

