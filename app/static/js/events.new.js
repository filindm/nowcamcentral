/*
*   EventsNewCtrl
*/
angular.module('app').controller('EventsNewCtrl', 
    ['$scope', '$state', 'Event', 'Customer', 
    function($scope, $state, Event, Customer){

    $scope.submit = function(){
        Event.create($scope.event).then(function(res){
            $state.go('events_details', {id: res.data._id});
        }, function(err){
            $scope.error = err;
        })
    }

    Customer.getAll().then(function(res){
        console.dir(res);
        $scope.customers = res.data.items;
    }, function(err){
        $scope.error = err;
    })

}]);


