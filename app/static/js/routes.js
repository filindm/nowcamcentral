/*
*  Router
*/
angular.module('app').config(['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/login');

    $stateProvider

    .state('login', {
        url: '/login',
        templateUrl: 'static/partials/login.html',
        controller: 'LoginCtrl'
    })
    .state('main', {
        views: {
            '': {
                templateUrl: 'static/partials/main.html'
            }
        }
    })
    .state('main.customers', {
        url: '/customers',
        views: {
            '': {
                templateUrl: 'static/partials/customers.html',
                controller: 'CustomersCtrl'
            },
            search: {
                templateUrl: 'static/partials/search.html',
                controller: 'CustomersSearchCtrl'
            }
        }
    })
    .state('customers_new', {
        url: '/customers/new',
        templateUrl: 'static/partials/customers.new.html',
        controller: 'CustomersNewCtrl'
    })
    .state('customers_details', {
        url: '/customers/:id',
        templateUrl: 'static/partials/customers.details.html',
        controller: 'CustomersDetailsCtrl'
    })
    .state('units', {
        url: '/units',
        templateUrl: 'static/partials/units.html',
        controller: 'UnitsCtrl'
    })
    .state('units_new', {
        url: '/units/new',
        templateUrl: 'static/partials/units.new.html',
        controller: 'UnitsNewCtrl'
    })
    .state('units_details', {
        url: '/units/:id',
        templateUrl: 'static/partials/units.details.html',
        controller: 'UnitsDetailsCtrl'
    })
    .state('main.events', {
        url: '/events',
        views: {
            '': {
                templateUrl: 'static/partials/events.html',
                controller: 'EventsCtrl'
            },
            search: {
                templateUrl: 'static/partials/search.html',
                controller: 'EventsSearchCtrl'
            }
        }
    })
    .state('events_new', {
        url: '/events/new',
        templateUrl: 'static/partials/events.new.html',
        controller: 'EventsNewCtrl'
    })
    .state('events_details', {
        url: '/events/:id',
        templateUrl: 'static/partials/events.details.html',
        controller: 'EventsDetailsCtrl'
    })
    .state('events_details_photos', {
        url: '/events/:id/photos',
        templateUrl: 'static/partials/events.photos.html',
        controller: 'EventsPhotosCtrl'
    })
    .state('events_details_tickets', {
        url: '/events/:id/tickets',
        templateUrl: 'static/partials/events.tickets.html',
        controller: 'EventsTicketsCtrl'
    })
    .state('events_details_microsite', {
        url: '/events/:id/microsite',
        templateUrl: 'static/partials/events.microsite.html',
        controller: 'EventsMicrositeCtrl'
    })
    .state('events_details_stat', {
        url: '/events/:id/stat',
        templateUrl: 'static/partials/events.stat.html',
        controller: 'EventsStatCtrl'
    })
    .state('events_details_overlay', {
        url: '/events/:id/overlay',
        templateUrl: 'static/partials/events.overlay.html',
        controller: 'EventsOverlayCtrl'
    })
    .state('settings', {
        url: '/settings',
        templateUrl: 'static/partials/settings.html',
        controller: 'SettingsCtrl'
    })

}]);

