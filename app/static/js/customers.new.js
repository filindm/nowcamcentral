/*
*   CustomersNewCtrl
*/
angular.module('app').controller('CustomersNewCtrl', 
    ['$scope', '$state', 'Customer', function($scope, $state, Customer){

    $scope.submit = function(){
        var customer = {
            name:           $scope.customer.name || '',
            contact_name:   $scope.customer.contact_name || '',
            contact_phone:  $scope.customer.contact_phone || '',
            contact_email:  $scope.customer.contact_email || '',
            file:           $scope.customer.logo ? $scope.customer.logo.file : null
        }
        Customer.create(customer).then(function(res){
            $state.go('customers_details', {id: res.data._id});
        }, function(err){
            $scope.error = err;
        })
    }

}]);


