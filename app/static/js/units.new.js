/*
*   UnitsNewCtrl
*/
angular.module('app').controller('UnitsNewCtrl', 
    ['$scope', '$state', 'Unit', function($scope, $state, Unit){

    $scope.submit = function(){
        Unit.create($scope.unit).then(function(res){
            $state.go('units_details', {id: res.data._id});
        }, function(err){
            console.dir(err);
            $scope.error = err;
        })
    }

}]);


