/*
*   EventsMicrositeCtrl
*/
angular.module('app').controller('EventsMicrositeCtrl', 
    ['$scope', '$stateParams', '$sce', '$window', '$http', '$location', 'Event', 'Photo',
    function($scope, $stateParams, $sce, $window, $http, $location, Event, Photo){

    $scope.fonts = [
        'Montserrat, sans-serif',
        'Georgia, serif',
        '"Palatino Linotype", "Book Antiqua", Palatino, serif',
        '"Times New Roman", Times, serif',
        'Arial, Helvetica, sans-serif',
        '"Arial Black", Gadget, sans-serif',
        '"Comic Sans MS", cursive, sans-serif',
        'Impact, Charcoal, sans-serif',
        '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
        'Tahoma, Geneva, sans-serif',
        '"Trebuchet MS", Helvetica, sans-serif',
        'Verdana, Geneva, sans-serif',
        '"Courier New", Courier, monospace',
        '"Lucida Console", Monaco, monospace'
    ]
    $scope.sizes = [
        '8px',
        '9px',
        '10px',
        '11px',
        '12px',
        '13px',
        '14px',
        '15px',
        '16px',
        '17px',
        '18px',
        '19px',
        '20px',
        '21px',
        '22px',
        '23px',
        '24px',
        '25px',
        '26px',
        '27px',
        '28px',
        '29px',
        '30px',
        '31px',
        '32px',
        '33px',
        '34px',
        '35px',
        '36px',
        '37px',
        '38px',
        '39px',
        '40px',
        '41px',
        '42px',
        '43px',
        '44px',
        '45px',
        '46px',
        '47px',
        '48px',
        '49px',
        '50px',
    ]

    $scope.tinymceOptions = {
        inline: true,
        theme: 'modern',
        plugins: [
            'link textcolor',
            // 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            // 'searchreplace wordcount visualblocks visualchars code fullscreen',
            // 'insertdatetime media nonbreaking save table contextmenu directionality',
            // 'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        menu: {
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link media | template hr'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            // table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            // tools: {title: 'Поля', items: 'csinsertfield csinsertlist'},
        },
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor',
        // toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        // toolbar2: 'print preview media | forecolor backcolor emoticons',
    }

    $scope.file_upload = {
        microsite_logo: {},
        microsite_bkg: {}
    }

    $scope.ui_state = {
        saving: false
    }
    
    $scope.$watch('file_upload.microsite_logo', function(data){
        // console.log('file_upload.microsite_logo watch, data.file: ' + data.file);
        // console.log('file_upload.microsite_logo watch, data.remove: ' + data.remove);
        updateIFrameLogo(data ? data.url : null);
    });
    
    $scope.$watch('file_upload.microsite_bkg', function(data){
        // console.log('file_upload.microsite_bkg watch, data.file: ' + data.file);
        // console.log('file_upload.microsite_bkg watch, data.remove: ' + data.remove);
        var url = null;
        if(data.remove){
            url = 'nodata';
        } else if(data.url){
            url = data.url;
        }
        if(url){
            updateIFrameBkg(url);
        }
    });

    $scope.$watch('event', function(data){
        updateIFrameData(data);
    }, true);

    $scope.iframeLoadedCallBack = function(){
        Event.get($stateParams.id).then(function(res){
            $scope.event = res.data;
        }, function(err){
            $scope.error = err;
        });
        Photo.getAll($stateParams.id, 0, 1).then(function(res){
            if(res.data.items !== undefined){
                setPhotos(res.data.items);
            }
        }, function(err){
            $scope.err = err;
        });
    }

    $scope.micrositeUrl = function(){
        return $scope.event ? 
            $sce.trustAsResourceUrl('/microsite#/init/' + $scope.event._id) :
            ''
    }

    $scope.micrositeAbsUrl = function(){
        if($scope.event){
            var url = $location.protocol() + '://' + $location.host();
            if(!( $location.protocol() === 'http'  && $location.port() ===  '80' || 
                $location.protocol() === 'https' && $location.port() === '443' )){
                url += ':' + $location.port(); 
            }
            url += '/microsite#/init/' + $scope.event._id;
            return $sce.trustAsResourceUrl(url);
        }
        return '';
    }

    $scope.preview = function(){
        window.open($scope.micrositeUrl());
    }

    $scope.toggleMobileMode = function(){
        $scope.isMobile = !$scope.isMobile;
    }

    $scope.isMobileMode = function(){
        return $scope.isMobile;
    }

    $scope.submit = function(){
        // console.log('submit');
        $scope.editable = false; // ???
        $scope.error = undefined;
        $scope.ui_state.saving = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.microsite_logo){
                if($scope.file_upload.microsite_logo.remove){
                    Event.setMicrositeLogo($scope.event._id, '');
                } else if($scope.file_upload.microsite_logo.file){
                    Event.setMicrositeLogo($scope.event._id, $scope.file_upload.microsite_logo.file);                    
                }
            }
            if($scope.file_upload.microsite_bkg){
                if($scope.file_upload.microsite_bkg.remove){
                    Event.setMicrositeBkg($scope.event._id, '');
                } else if($scope.file_upload.microsite_bkg.file){
                    Event.setMicrositeBkg($scope.event._id, $scope.file_upload.microsite_bkg.file);                    
                }
            }
            $scope.ui_state.saving = false; // Other async actions may be in progress!
            // window.setTimeout(function(){$scope.ui_state.saving = false;$scope.$apply();}, 3000)
        }, function(err){
            $scope.editable = false; // ???
            $scope.error = err;
            $scope.ui_state.saving = false;
        })
    }

    $scope.getLogoImgSrc = function(){
        if($scope.file_upload.microsite_logo && $scope.file_upload.microsite_logo.url){
            return $scope.file_upload.microsite_logo.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.microsite_logo.remove){
            return '/events/' + $scope.event._id + '/microsite_logo'
        }
        return 'static/img/avatar.png';
    }

    $scope.getBkgImgSrc = function(){
        if($scope.file_upload.microsite_bkg && $scope.file_upload.microsite_bkg.url){
            return $scope.file_upload.microsite_bkg.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.microsite_bkg.remove){
            return '/events/' + $scope.event._id + '/microsite_bkg'
        }
        return 'static/img/avatar.png';
    }

    $scope.removeLogo = function(){
        // console.log('removeLogo start, file: ' + $scope.file_upload.microsite_logo.file);
        // console.log('removeLogo start, remove: ' + $scope.file_upload.microsite_logo.remove);
        if($scope.file_upload.microsite_logo){
            $scope.file_upload.microsite_logo = {
                file: null,
                url: null,
                remove: true
            }
        }
        // console.log('removeLogo end, file: ' + $scope.file_upload.microsite_logo.file);
        // console.log('removeLogo end, remove: ' + $scope.file_upload.microsite_logo.remove);
    }

    $scope.isRemoveLogoVisible = function(){
        return !!$scope.file_upload.microsite_logo.url || ($scope.event && !!$scope.event.microsite_logo);
    }

    $scope.removeBkg = function(){
        // console.log('removeBkg start, file: ' + $scope.file_upload.microsite_bkg.file);
        // console.log('removeBkg start, remove: ' + $scope.file_upload.microsite_bkg.remove);
        if($scope.file_upload.microsite_bkg){
            $scope.file_upload.microsite_bkg = {
                file: null,
                url: null,
                remove: true
            }
        }
    }

    $scope.isRemoveBkgVisible = function(){
        return !!$scope.file_upload.microsite_bkg.url || ($scope.event && !!$scope.event.microsite_bkg);
    }

    $scope.activatePage = function(path){
        $('iframe')[0].contentWindow.postMessage({
            action: 'activatePage',
            path: path
        }, '*')
    }

    $scope.addDataField = function(){
        // console.log('addDataField');
        $scope.event.microsite_data_fields = $scope.event.microsite_data_fields || [];
        $scope.event.microsite_data_fields.push({}); 
    }

    $scope.download_user_data = function(){
        $http.get('/events/' + $scope.event._id + '/user_data').then(function(res){
            // var file = new Blob([res.data], {type: 'text/csv', 'name': 'user_data.csv'});
            // var fileURL = URL.createObjectURL(file);
            // $window.open(fileURL);
            var a = document.createElement("a");
            //document.body.appendChild(a);
            a.style = "display: none";
            var blob = new Blob([res.data], {type: "text/csv"});
            var url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = 'user_data.csv';
            a.click();
            window.URL.revokeObjectURL(url);
            child.parentNode.removeChild(a);
        })
    }

    function setPhotos(photos){
        // console.log('setSelectedPhoto, photo: ', photo);
        $('iframe')[0].contentWindow.postMessage({
            action: 'setPhotos',
            photos: photos
        }, '*')
    }

    function updateIFrameLogo(url){
        // console.log('updateIFrameLogo');
        $('iframe')[0].contentWindow.postMessage({
            action: 'setLogo',
            url: url
        }, '*')
    }

    function updateIFrameBkg(url){
        // console.log('updateIFrameBkg');
        $('iframe')[0].contentWindow.postMessage({
            action: 'setBkg',
            url: url
        }, '*')
    }

    function updateIFrameData(data){
        // console.log('updateIFrameData, data: ' + data);
        var ang = $('iframe')[0].contentWindow.angular;
        if(ang){
            var scope = ang.element('body').scope();
            scope.event = data;
            scope.$apply();            
        } else {
            console.warn('angular not found in microsite iframe')
        }
    }

}]);


angular.module('app').directive('iframeOnload', [function(){
    return {
        scope: {
            callBack: '&iframeOnload'
        },
        link: function(scope, element, attrs){
            element.on('load', function(){
                return scope.callBack();
            })
        }
    }
}]);

angular.module('app').directive('nowcamFontSetting', function(){
    return {
        scope: {
            title: '@',
            data: '=',
            fonts: '=',
            sizes: '='
        },
        templateUrl: 'static/partials/events.microsite.toolbar.font.html'
    }
})


