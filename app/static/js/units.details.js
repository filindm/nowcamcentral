/*
*   UnitsDetailsCtrl
*/
angular.module('app').controller('UnitsDetailsCtrl', 
    ['$scope', '$stateParams', 'Unit', function($scope, $stateParams, Unit){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        Unit.update($scope.unit).then(function(res){
            //alert('ok!');
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        // console.dir($scope.theForm);
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Unit.get($stateParams.id).then(function(resp){
        $scope.unit = resp.data;
    }, function(err){
        $scope.error = err;
    })


}]);


