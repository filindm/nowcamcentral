/*
*   EventsStatCtrl
*/
angular.module('app').controller('EventsStatCtrl', 
    ['$scope', '$stateParams', 'Event',
    function($scope, $stateParams, Event){
    
    Event.get($stateParams.id).then(function(res){
        $scope.event = res.data;
    }, function(err){
        $scope.error = err;
    });

    Event.stat($stateParams.id).then(function(res){
        $scope.stat = res.data;
    }, function(err){
        $scope.error = err;
    });

}]);


