/*
*   PhotosApi
*/
angular.module('app').service('Photo', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
    }
    var cfg = {
        baseUrl: '/photos',
    }

    return {

        getAll: function(event_id, skip, limit, code){
            var params = {
                limit: limit,
                skip: skip, 
                event_id: event_id
            }
            if(code){
                params['code'] = code;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        /**
            photos - a object with 'items' property containing an array of photo objects
        */
        bulkUpdate: function(photos){
            return $http.put(cfg.baseUrl, photos);
        }
    }
}])

