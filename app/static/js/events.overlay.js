/*
*   EventsOverlayCtrl
*/
angular.module('app').controller('EventsOverlayCtrl', 
    ['$scope', '$http', '$stateParams', 'Event', 'Photo',
    function($scope, $http, $stateParams, Event, Photo){

    $scope.file_upload = {
        overlay: {}
    }

    $scope.ui_state = {
        action_in_progress: false
    }

    $scope.save = function(){
        $scope.ui_state.action_in_progress = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.overlay){
                if($scope.file_upload.overlay.remove){
                    Event.setOverlay($scope.event._id, '');
                } else if($scope.file_upload.overlay.file){
                    console.dir($scope.file_upload.overlay.file);
                    Event.setOverlay($scope.event._id, $scope.file_upload.overlay.file);                    
                }
            }
            $scope.ui_state.action_in_progress = false;

        }, function(err){
            $scope.ui_state.action_in_progress = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        console.log('=== cancel');
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
    }

    $scope.getOverlayImgSrc = function(){
        if($scope.file_upload.overlay && $scope.file_upload.overlay.url){
            return $scope.file_upload.overlay.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.overlay.remove){
            return '/events/' + $scope.event._id + '/overlay'
        }
        return 'static/img/avatar.png';        
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });


}]);


