/*
*   EventsApi
*/
// angular.module('app').factory('EventsApi', ['$http', function($http){
angular.module('app').service('Event', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
        // withCredentials: true
    }
    var cfg = {
        baseUrl: '/events',
        //loginId: 'john.smith',
        //tenantUrl: 'new.brightpattern.com'
    }

    return {

        getAll: function(skip, limit, name){
            var params = {
                    limit: limit,
                    skip: skip
            }
            if(name){
                params.name = name;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(data){
            return $http({
                method: 'POST',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        update: function(data){
            return $http({
                method: 'PUT',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        },

        stat: function(id){
            return $http.get(cfg.baseUrl + '/' + id + '/stat', httpCfg);
        },

        setMicrositeLogo: function(eventId, file){
            var formData = new FormData();
            formData.append('microsite_logo', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/microsite_logo',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setMicrositeBkg: function(eventId, file){
            var formData = new FormData();
            formData.append('microsite_bkg', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/microsite_bkg',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setTicketBody: function(eventId, file){
            var formData = new FormData();
            formData.append('ticket_body', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/ticket_body',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setOverlay: function(eventId, file){
            var formData = new FormData();
            formData.append('overlay', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/overlay',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

    }
}])

