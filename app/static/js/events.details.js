/*
*   EventsDetailsCtrl
*/
angular.module('app').controller('EventsDetailsCtrl', 
    ['$scope', '$stateParams', 'Event', 'Customer',
    function($scope, $stateParams, Event, Customer){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        $scope.error = undefined;
        Event.update($scope.event).then(function(res){
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });

    Customer.getAll().then(function(res){
        $scope.customers = res.data.items;
    }, function(err){
        $scope.error = err;
    })

}]);


