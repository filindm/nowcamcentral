/*
*   UnitsApi
*/
angular.module('app').service('Unit', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
        // withCredentials: true
    }
    var cfg = {
        baseUrl: '/units',
        //loginId: 'john.smith',
        //tenantUrl: 'new.brightpattern.com'
    }

    return {

        getAll: function(skip, limit){
            return $http.get(cfg.baseUrl, {
                params: {
                    limit: limit,
                    skip: skip
                }
            }, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(event){
            return $http.post(cfg.baseUrl, event, httpCfg);
        },

        update: function(event){
            return $http.put(cfg.baseUrl, event, httpCfg);
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        }
    }
}])

