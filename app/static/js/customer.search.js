/*
*   CustomersSearchCtrl
*/
angular.module('app').factory('CustomersSearch', function(){
    return {filter: ''}
});

angular.module('app').controller('CustomersSearchCtrl', 
    ['$scope', '$state', 'CustomersSearch', function($scope, $state, CustomersSearch){

    $scope.placeholder = 'Search for customer';
    $scope.search = CustomersSearch;

}]);

