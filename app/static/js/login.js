/*
*   LoginCtrl
*/
angular.module('app').controller('LoginCtrl', 
    ['$scope', '$window', '$state', 'AuthService', 
    function($scope, $window, $state, AuthService){

    $scope.email = undefined;
    $scope.password = undefined;

    $scope.login = function(){
        AuthService.login($scope.username, $scope.password).then(function(){
            console.log('successful login, goto customers');
            // $location.url('customers');
            $state.go('main.customers');
        }, function(err){
            $window.alert(err);
        })
    }

}]);

