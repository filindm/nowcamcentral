/*
*   Application
*/
angular.module('app', [
    'ui.router',
    'mgcrea.ngStrap',
    // 'minicolors'
    'colorpicker.module'
]);

angular.module('app').run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
});

/*
*   AppCtrl
*/
angular.module('app').controller('AppCtrl', 
    ['$scope', '$location', function($scope, $location){


}]);
/*
*   Auth
*/
angular.module('app').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('AuthInjector');
}]);

angular.module('app').factory('AuthInjector', 
    ['$location', '$q', 'AuthService', function($location, $q, AuthService){
    return {
        request: function(config) {
            var authToken = AuthService.token();
            if(authToken){
                config.headers['Authorization'] = 'JWT ' + authToken;
            }
            return config;
        },
        // response: function (response) {
        //     return response || $q.when(response);
        // },
        responseError: function (response) {
            if (response.status === 401) {
                $location.url('login');
                // //here I preserve login page 
                // someService
                //    .setRestrictedPageBeforeLogin(
                //             extractPreservedInfoAboutPage(response)
                //     )
                // $rootScope.$broadcast('error')
            }
            return $q.reject(response);
            //return response.reject();
        }
    };
}]);

angular.module('app').factory('AuthService', 
    ['$injector', '$q', function($injector, $q){
    return {
        login: function(username, password){
            var defer = $q.defer();
            // console.log('username: ' + username);
            $injector.get("$http").post('/auth', {
                'username': username,
                'password': password
            }).then(function(res){
                // console.log('Authentication successful, data: ');
                // console.dir(data);
                sessionStorage.authToken = res.data.access_token;
                sessionStorage.username = username;
                defer.resolve();
            }, function(err){
                defer.reject('Authentication failed, error: ' + err);
            });
            return defer.promise;
        },
        token: function(){
            return sessionStorage.authToken;
        }
    };
}]);

/*
*   CustomersSearchCtrl
*/
angular.module('app').factory('CustomersSearch', function(){
    return {filter: ''}
});

angular.module('app').controller('CustomersSearchCtrl', 
    ['$scope', '$state', 'CustomersSearch', function($scope, $state, CustomersSearch){

    $scope.placeholder = 'Search for customer';
    $scope.search = CustomersSearch;

}]);

/*
*   CustomersApi
*/
angular.module('app').service('Customer', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
    }
    var cfg = {
        baseUrl: '/customers',
    }

    return {

        getAll: function(skip, limit, name){
            var params = {
                    limit: limit,
                    skip: skip
            }
            if(name){
                params.name = name;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(data){
            return $http({
                method: 'POST',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        update: function(data){
            return $http({
                method: 'PUT',
                url: cfg.baseUrl,
                data: JSON.stringify(data),
            });
        },

        set_logo: function(id, file){
            var formData = new FormData();
            formData.append('logo', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + id + '/logo',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            })
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        }
    }
}])

/*
*   CustomersDetailsCtrl
*/
angular.module('app').controller('CustomersDetailsCtrl', 
    ['$scope', '$stateParams', 'Customer', function($scope, $stateParams, Customer){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        // var customer = {
        //     id:             $scope.customer.id,
        //     name:           $scope.customer.name || '',
        //     contact_name:   $scope.customer.contact_name || '',
        //     contact_phone:  $scope.customer.contact_phone || '',
        //     contact_email:  $scope.customer.contact_email || '',
        //     file:           $scope.customer.logo ? $scope.customer.logo.file : null
        // }
        Customer.update($scope.customer).then(function(res){
            if($scope.customer.logo && $scope.customer.logo.file){
                console.log('_id: ' + res.data._id + ', file: ' + $scope.customer.logo.file);
                console.dir($scope.customer.logo.file);
                Customer.set_logo(res.data._id, $scope.customer.logo.file);
            }
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        // console.dir($scope.theForm);
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Customer.get($stateParams.id).then(function(resp){
        $scope.customer = resp.data;
    }, function(err){
        $scope.error = err;
    })


}]);


/*
*   CustomersCtrl
*/
angular.module('app').controller('CustomersCtrl', 
    ['$scope', '$timeout', '$state', 'Customer', 'CustomersSearch', 
    function($scope, $timeout, $state, Customer, CustomersSearch){

    var limit = 10;

    $scope.customers = {
        items: [],
        total: 0,
        skip: 0,
        events_count: {},
    };

    $scope.search = CustomersSearch;

    $scope.$watch('search.filter', function(newValue){
        //console.log('watch filter: ' + newValue);
        // if(newValue.length > 2 || newValue.length == 0){
        //     console.log('timeout load');
        //     $timeout(load, 500);
        // }
        $timeout(load, 500);
    })

    $scope.onAddCustomerBtnClicked = function(){
        $state.go('customers_new');
    }

    var load_running = false;
    function load(){
        if(load_running) return;
        load_running = true;
        Customer.getAll($scope.skip, limit, $scope.search.filter)
        .then(function(res){
            $scope.customers.items = res.data.items;
            $scope.customers.total = res.data.total;
            $scope.customers.events_count = res.data.events_count;
        }, function(err){
            console.error(err);
        })
        .finally(function(){
            load_running = false;
        }) 
    }

    load();

}]);


/*
*   CustomersNewCtrl
*/
angular.module('app').controller('CustomersNewCtrl', 
    ['$scope', '$state', 'Customer', function($scope, $state, Customer){

    $scope.submit = function(){
        var customer = {
            name:           $scope.customer.name || '',
            contact_name:   $scope.customer.contact_name || '',
            contact_phone:  $scope.customer.contact_phone || '',
            contact_email:  $scope.customer.contact_email || '',
            file:           $scope.customer.logo ? $scope.customer.logo.file : null
        }
        Customer.create(customer).then(function(res){
            $state.go('customers_details', {id: res.data._id});
        }, function(err){
            $scope.error = err;
        })
    }

}]);


/*
*   EventsSearchCtrl
*/
angular.module('app').factory('EventsSearch', function(){
    return {filter: ''}
});

angular.module('app').controller('EventsSearchCtrl', 
    ['$scope', '$state', 'EventsSearch', function($scope, $state, EventsSearch){

    $scope.placeholder = 'Search for event';
    $scope.search = EventsSearch;

}]);

/*
*   EventsApi
*/
// angular.module('app').factory('EventsApi', ['$http', function($http){
angular.module('app').service('Event', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
        // withCredentials: true
    }
    var cfg = {
        baseUrl: '/events',
        //loginId: 'john.smith',
        //tenantUrl: 'new.brightpattern.com'
    }

    return {

        getAll: function(skip, limit, name){
            var params = {
                    limit: limit,
                    skip: skip
            }
            if(name){
                params.name = name;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(data){
            return $http({
                method: 'POST',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        update: function(data){
            return $http({
                method: 'PUT',
                url: cfg.baseUrl,
                data: JSON.stringify(data)
            });
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        },

        stat: function(id){
            return $http.get(cfg.baseUrl + '/' + id + '/stat', httpCfg);
        },

        setMicrositeLogo: function(eventId, file){
            var formData = new FormData();
            formData.append('microsite_logo', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/microsite_logo',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setMicrositeBkg: function(eventId, file){
            var formData = new FormData();
            formData.append('microsite_bkg', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/microsite_bkg',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setTicketBody: function(eventId, file){
            var formData = new FormData();
            formData.append('ticket_body', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/ticket_body',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

        setOverlay: function(eventId, file){
            var formData = new FormData();
            formData.append('overlay', file);
            return $http({
                method: 'POST',
                url: cfg.baseUrl + '/' + eventId + '/overlay',
                headers: {
                    'Content-Type': undefined
                },
                data: formData
            });
        },

    }
}])

/*
*   EventsDetailsCtrl
*/
angular.module('app').controller('EventsDetailsCtrl', 
    ['$scope', '$stateParams', 'Event', 'Customer',
    function($scope, $stateParams, Event, Customer){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        $scope.error = undefined;
        Event.update($scope.event).then(function(res){
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });

    Customer.getAll().then(function(res){
        $scope.customers = res.data.items;
    }, function(err){
        $scope.error = err;
    })

}]);


/*
*   EventsCtrl
*/
angular.module('app').controller('EventsCtrl', 
    ['$scope', '$timeout', '$state', 'Event', 'EventsSearch', 
    function($scope, $timeout, $state, Event, EventsSearch){

    var limit = 100;

    $scope.events = {
        items: [],
        total: 0,
        skip: 0
    };

    $scope.search = EventsSearch;

    $scope.$watch('search.filter', function(newValue){
        //console.log('watch filter: ' + newValue);
        // if(newValue.length > 2 || newValue.length == 0){
        //     console.log('timeout load');
        //     $timeout(load, 500);
        // }
        $timeout(load, 500);
    })    

    $scope.onAddEventBtnClicked = function(){
        $state.go('events_new');
    }

    var load_running = false;
    function load(){
        if(load_running) return;
        load_running = true;
        Event.getAll($scope.skip, limit, $scope.search.filter)
        .then(function(res){
            $scope.events.items = res.data.items;
            $scope.events.total = res.data.total;
        }, function(err){
            console.error(err);
        })
        .finally(function(){
            load_running = false;
        }) 
    }

    load();

}]);


/*
*   EventsMicrositeCtrl
*/
angular.module('app').controller('EventsMicrositeCtrl', 
    ['$scope', '$stateParams', '$sce', '$window', '$http', '$location', 'Event', 'Photo',
    function($scope, $stateParams, $sce, $window, $http, $location, Event, Photo){

    $scope.fonts = [
        'Montserrat, sans-serif',
        'Georgia, serif',
        '"Palatino Linotype", "Book Antiqua", Palatino, serif',
        '"Times New Roman", Times, serif',
        'Arial, Helvetica, sans-serif',
        '"Arial Black", Gadget, sans-serif',
        '"Comic Sans MS", cursive, sans-serif',
        'Impact, Charcoal, sans-serif',
        '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
        'Tahoma, Geneva, sans-serif',
        '"Trebuchet MS", Helvetica, sans-serif',
        'Verdana, Geneva, sans-serif',
        '"Courier New", Courier, monospace',
        '"Lucida Console", Monaco, monospace'
    ]
    $scope.sizes = [
        '8px',
        '9px',
        '10px',
        '11px',
        '12px',
        '13px',
        '14px',
        '15px',
        '16px',
        '17px',
        '18px',
        '19px',
        '20px',
        '21px',
        '22px',
        '23px',
        '24px',
        '25px',
        '26px',
        '27px',
        '28px',
        '29px',
        '30px',
        '31px',
        '32px',
        '33px',
        '34px',
        '35px',
        '36px',
        '37px',
        '38px',
        '39px',
        '40px',
        '41px',
        '42px',
        '43px',
        '44px',
        '45px',
        '46px',
        '47px',
        '48px',
        '49px',
        '50px',
    ]

    $scope.tinymceOptions = {
        inline: true,
        theme: 'modern',
        plugins: [
            'link textcolor',
            // 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            // 'searchreplace wordcount visualblocks visualchars code fullscreen',
            // 'insertdatetime media nonbreaking save table contextmenu directionality',
            // 'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        menu: {
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link media | template hr'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            // table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            // tools: {title: 'Поля', items: 'csinsertfield csinsertlist'},
        },
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor',
        // toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        // toolbar2: 'print preview media | forecolor backcolor emoticons',
    }

    $scope.file_upload = {
        microsite_logo: {},
        microsite_bkg: {}
    }

    $scope.ui_state = {
        saving: false
    }
    
    $scope.$watch('file_upload.microsite_logo', function(data){
        // console.log('file_upload.microsite_logo watch, data.file: ' + data.file);
        // console.log('file_upload.microsite_logo watch, data.remove: ' + data.remove);
        updateIFrameLogo(data ? data.url : null);
    });
    
    $scope.$watch('file_upload.microsite_bkg', function(data){
        // console.log('file_upload.microsite_bkg watch, data.file: ' + data.file);
        // console.log('file_upload.microsite_bkg watch, data.remove: ' + data.remove);
        var url = null;
        if(data.remove){
            url = 'nodata';
        } else if(data.url){
            url = data.url;
        }
        if(url){
            updateIFrameBkg(url);
        }
    });

    $scope.$watch('event', function(data){
        updateIFrameData(data);
    }, true);

    $scope.iframeLoadedCallBack = function(){
        Event.get($stateParams.id).then(function(res){
            $scope.event = res.data;
        }, function(err){
            $scope.error = err;
        });
        Photo.getAll($stateParams.id, 0, 1).then(function(res){
            if(res.data.items !== undefined){
                setPhotos(res.data.items);
            }
        }, function(err){
            $scope.err = err;
        });
    }

    $scope.micrositeUrl = function(){
        return $scope.event ? 
            $sce.trustAsResourceUrl('/microsite#/init/' + $scope.event._id) :
            ''
    }

    $scope.micrositeAbsUrl = function(){
        if($scope.event){
            var url = $location.protocol() + '://' + $location.host();
            if(!( $location.protocol() === 'http'  && $location.port() ===  '80' || 
                $location.protocol() === 'https' && $location.port() === '443' )){
                url += ':' + $location.port(); 
            }
            url += '/microsite#/init/' + $scope.event._id;
            return $sce.trustAsResourceUrl(url);
        }
        return '';
    }

    $scope.preview = function(){
        window.open($scope.micrositeUrl());
    }

    $scope.toggleMobileMode = function(){
        $scope.isMobile = !$scope.isMobile;
    }

    $scope.isMobileMode = function(){
        return $scope.isMobile;
    }

    $scope.submit = function(){
        // console.log('submit');
        $scope.editable = false; // ???
        $scope.error = undefined;
        $scope.ui_state.saving = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.microsite_logo){
                if($scope.file_upload.microsite_logo.remove){
                    Event.setMicrositeLogo($scope.event._id, '');
                } else if($scope.file_upload.microsite_logo.file){
                    Event.setMicrositeLogo($scope.event._id, $scope.file_upload.microsite_logo.file);                    
                }
            }
            if($scope.file_upload.microsite_bkg){
                if($scope.file_upload.microsite_bkg.remove){
                    Event.setMicrositeBkg($scope.event._id, '');
                } else if($scope.file_upload.microsite_bkg.file){
                    Event.setMicrositeBkg($scope.event._id, $scope.file_upload.microsite_bkg.file);                    
                }
            }
            $scope.ui_state.saving = false; // Other async actions may be in progress!
            // window.setTimeout(function(){$scope.ui_state.saving = false;$scope.$apply();}, 3000)
        }, function(err){
            $scope.editable = false; // ???
            $scope.error = err;
            $scope.ui_state.saving = false;
        })
    }

    $scope.getLogoImgSrc = function(){
        if($scope.file_upload.microsite_logo && $scope.file_upload.microsite_logo.url){
            return $scope.file_upload.microsite_logo.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.microsite_logo.remove){
            return '/events/' + $scope.event._id + '/microsite_logo'
        }
        return 'static/img/avatar.png';
    }

    $scope.getBkgImgSrc = function(){
        if($scope.file_upload.microsite_bkg && $scope.file_upload.microsite_bkg.url){
            return $scope.file_upload.microsite_bkg.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.microsite_bkg.remove){
            return '/events/' + $scope.event._id + '/microsite_bkg'
        }
        return 'static/img/avatar.png';
    }

    $scope.removeLogo = function(){
        // console.log('removeLogo start, file: ' + $scope.file_upload.microsite_logo.file);
        // console.log('removeLogo start, remove: ' + $scope.file_upload.microsite_logo.remove);
        if($scope.file_upload.microsite_logo){
            $scope.file_upload.microsite_logo = {
                file: null,
                url: null,
                remove: true
            }
        }
        // console.log('removeLogo end, file: ' + $scope.file_upload.microsite_logo.file);
        // console.log('removeLogo end, remove: ' + $scope.file_upload.microsite_logo.remove);
    }

    $scope.isRemoveLogoVisible = function(){
        return !!$scope.file_upload.microsite_logo.url || ($scope.event && !!$scope.event.microsite_logo);
    }

    $scope.removeBkg = function(){
        // console.log('removeBkg start, file: ' + $scope.file_upload.microsite_bkg.file);
        // console.log('removeBkg start, remove: ' + $scope.file_upload.microsite_bkg.remove);
        if($scope.file_upload.microsite_bkg){
            $scope.file_upload.microsite_bkg = {
                file: null,
                url: null,
                remove: true
            }
        }
    }

    $scope.isRemoveBkgVisible = function(){
        return !!$scope.file_upload.microsite_bkg.url || ($scope.event && !!$scope.event.microsite_bkg);
    }

    $scope.activatePage = function(path){
        $('iframe')[0].contentWindow.postMessage({
            action: 'activatePage',
            path: path
        }, '*')
    }

    $scope.addDataField = function(){
        // console.log('addDataField');
        $scope.event.microsite_data_fields = $scope.event.microsite_data_fields || [];
        $scope.event.microsite_data_fields.push({}); 
    }

    $scope.download_user_data = function(){
        $http.get('/events/' + $scope.event._id + '/user_data').then(function(res){
            // var file = new Blob([res.data], {type: 'text/csv', 'name': 'user_data.csv'});
            // var fileURL = URL.createObjectURL(file);
            // $window.open(fileURL);
            var a = document.createElement("a");
            //document.body.appendChild(a);
            a.style = "display: none";
            var blob = new Blob([res.data], {type: "text/csv"});
            var url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = 'user_data.csv';
            a.click();
            window.URL.revokeObjectURL(url);
            child.parentNode.removeChild(a);
        })
    }

    function setPhotos(photos){
        // console.log('setSelectedPhoto, photo: ', photo);
        $('iframe')[0].contentWindow.postMessage({
            action: 'setPhotos',
            photos: photos
        }, '*')
    }

    function updateIFrameLogo(url){
        // console.log('updateIFrameLogo');
        $('iframe')[0].contentWindow.postMessage({
            action: 'setLogo',
            url: url
        }, '*')
    }

    function updateIFrameBkg(url){
        // console.log('updateIFrameBkg');
        $('iframe')[0].contentWindow.postMessage({
            action: 'setBkg',
            url: url
        }, '*')
    }

    function updateIFrameData(data){
        // console.log('updateIFrameData, data: ' + data);
        var ang = $('iframe')[0].contentWindow.angular;
        if(ang){
            var scope = ang.element('body').scope();
            scope.event = data;
            scope.$apply();            
        } else {
            console.warn('angular not found in microsite iframe')
        }
    }

}]);


angular.module('app').directive('iframeOnload', [function(){
    return {
        scope: {
            callBack: '&iframeOnload'
        },
        link: function(scope, element, attrs){
            element.on('load', function(){
                return scope.callBack();
            })
        }
    }
}]);

angular.module('app').directive('nowcamFontSetting', function(){
    return {
        scope: {
            title: '@',
            data: '=',
            fonts: '=',
            sizes: '='
        },
        templateUrl: 'static/partials/events.microsite.toolbar.font.html'
    }
})


/*
*   EventsNewCtrl
*/
angular.module('app').controller('EventsNewCtrl', 
    ['$scope', '$state', 'Event', 'Customer', 
    function($scope, $state, Event, Customer){

    $scope.submit = function(){
        Event.create($scope.event).then(function(res){
            $state.go('events_details', {id: res.data._id});
        }, function(err){
            $scope.error = err;
        })
    }

    Customer.getAll().then(function(res){
        console.dir(res);
        $scope.customers = res.data.items;
    }, function(err){
        $scope.error = err;
    })

}]);


/*
*   EventsOverlayCtrl
*/
angular.module('app').controller('EventsOverlayCtrl', 
    ['$scope', '$http', '$stateParams', 'Event', 'Photo',
    function($scope, $http, $stateParams, Event, Photo){

    $scope.file_upload = {
        overlay: {}
    }

    $scope.ui_state = {
        action_in_progress: false
    }

    $scope.save = function(){
        $scope.ui_state.action_in_progress = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.overlay){
                if($scope.file_upload.overlay.remove){
                    Event.setOverlay($scope.event._id, '');
                } else if($scope.file_upload.overlay.file){
                    console.dir($scope.file_upload.overlay.file);
                    Event.setOverlay($scope.event._id, $scope.file_upload.overlay.file);                    
                }
            }
            $scope.ui_state.action_in_progress = false;

        }, function(err){
            $scope.ui_state.action_in_progress = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        console.log('=== cancel');
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
    }

    $scope.getOverlayImgSrc = function(){
        if($scope.file_upload.overlay && $scope.file_upload.overlay.url){
            return $scope.file_upload.overlay.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.overlay.remove){
            return '/events/' + $scope.event._id + '/overlay'
        }
        return 'static/img/avatar.png';        
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });


}]);


/*
*   EventsPhotosCtrl
*/
angular.module('app').controller('EventsPhotosCtrl', 
    ['$scope', '$stateParams', 'Event', 'Photo',
    function($scope, $stateParams, Event, Photo){

    $scope.event = {};
    $scope.photos = {};
    $scope.code = '';

    $scope.ui_state = {
        action_in_progress: false
    }

    
    // Pagination

    var photos_per_page = 12;
    var max_pages_count = 15;
    $scope.current_page = 0;
    $scope.start_page = 0;

    $scope.total_pages = function(){
        return Math.ceil($scope.photos.total / photos_per_page);
    }

    $scope.has_prev = function(){
        return $scope.start_page > 0;
    }

    $scope.has_next = function(){
        return $scope.start_page + max_pages_count < $scope.total_pages();
    }

    $scope.enum_pages = function(){
        var res = [];
        for(var i = $scope.start_page; 
            i < $scope.start_page + max_pages_count && i < $scope.total_pages(); 
            i++){
            res.push(i);
        }
        return res;
    }

    $scope.prev = function(){
        if($scope.has_prev()){
            $scope.start_page -= max_pages_count;
            $scope.current_page = $scope.start_page + max_pages_count - 1;
            $scope.load_photos();
        }
    }

    $scope.next = function(){
        if($scope.has_next()){
            $scope.start_page += max_pages_count;
            $scope.current_page = $scope.start_page;
            $scope.load_photos();
        }
    }

    $scope.set_current_page = function(n){
        $scope.current_page = n;
        $scope.load_photos();
    }

    $scope.submit = function(){
        $scope.ui_state.action_in_progress = true;
        Photo.bulkUpdate($scope.photos).finally(function(){
            $scope.ui_state.action_in_progress = false;
        });
    }

    $scope.load_photos = function(){
        $scope.ui_state.action_in_progress = true;
        var skip = $scope.current_page * photos_per_page;
        var limit = photos_per_page;
        if($scope.code){
            skip = 0;
            limit = 1;
        }
        Photo.getAll($stateParams.id, skip, limit, $scope.code).then(function(res){
            $scope.photos = res.data;
        }, function(err){
            $scope.error = err;
        }).finally(function(){
            $scope.ui_state.action_in_progress = false;
        })
    }

    Event.get($stateParams.id).then(function(res){
        $scope.event = res.data;
    }, function(err){
        $scope.error = err;
    });

    $scope.load_photos();

}]);


/*
*   EventsStatCtrl
*/
angular.module('app').controller('EventsStatCtrl', 
    ['$scope', '$stateParams', 'Event',
    function($scope, $stateParams, Event){
    
    Event.get($stateParams.id).then(function(res){
        $scope.event = res.data;
    }, function(err){
        $scope.error = err;
    });

    Event.stat($stateParams.id).then(function(res){
        $scope.stat = res.data;
    }, function(err){
        $scope.error = err;
    });

}]);


/*
*   EventsTabs Directive
*/
angular.module('app').directive('eventsTabs', function(){
    return {
        templateUrl: 'static/partials/events.tabs.html'
    }
})
/*
*   EventsTicketsCtrl
*/
angular.module('app').controller('EventsTicketsCtrl', 
    ['$scope', '$http', '$stateParams', 'Event', 'Photo',
    function($scope, $http, $stateParams, Event, Photo){

    $scope.photos = [];
    $scope.photos_idx = 0;
    $scope.file_upload = {
        ticket_body: {}
    }

    $scope.ui_state = {
        action_in_progress: false
    }

    $scope.save = function(){
        $scope.ui_state.action_in_progress = true;
        Event.update($scope.event).then(function(res){
            if($scope.file_upload.ticket_body){
                if($scope.file_upload.ticket_body.remove){
                    Event.setTicketBody($scope.event._id, '');
                } else if($scope.file_upload.ticket_body.file){
                    console.dir($scope.file_upload.ticket_body.file);
                    Event.setTicketBody($scope.event._id, $scope.file_upload.ticket_body.file);                    
                }
            }
            $scope.ui_state.action_in_progress = false;

        }, function(err){
            $scope.ui_state.action_in_progress = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        console.log('=== cancel');
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
    }

    $scope.photos_next = function(){
        $scope.photos_idx = ($scope.photos_idx + 1) % $scope.photos.length;
    }

    $scope.getCodeInputBoxStyle = function(){
        if($scope.event){
            return {
                top: $scope.event.ticket_top + '%',
                left: $scope.event.ticket_left + '%',
                width: $scope.event.ticket_width + '%',
                height: $scope.event.ticket_height + '%'
            }
        } else {
            return {}
        }
    }

    $scope.getTicketImgSrc = function(){
        if($scope.file_upload.ticket_body && $scope.file_upload.ticket_body.url){
            return $scope.file_upload.ticket_body.url;
        }
        if($scope.event && $scope.event._id && !$scope.file_upload.ticket_body.remove){
            return '/events/' + $scope.event._id + '/ticket_body'
        }
        return 'static/img/avatar.png';        
    }

    Event.get($stateParams.id).then(function(resp){
        $scope.event = resp.data;
    }, function(err){
        $scope.error = err;
    });

    Photo.getAll($stateParams.id, 0, 20).then(function(res){
        $scope.photos = res.data.items;
    }, function(err){
        $scope.error = err;
    });

}]);


/**
* File Upload Directive
*/
angular.module('app').directive('fileUpload', function(){
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function link(scope, element, attrs, ngModel) {
            if(!ngModel) return;
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });
            read();

            function read() {
                // console.log('read');
                var file = element[0].files[0];
                if(!file) return;
                // console.dir(file);
                var reader = new FileReader();
                reader.onload = (function(f){
                    return function(e){
                        // console.dir('fileUpload: file: ' + JSON.stringify(f) + ', url: ' + e.target.result);
                        // console.dir(ngModel);
                        // console.dir(ngModel.$modelValue);
                        // console.dir(ngModel.$viewValue);
                        ngModel.$setViewValue({
                            file: f,
                            url: e.target.result,
                            remove: false
                        });
                        element[0].value = null;
                    }
                })(file);
                reader.readAsDataURL(file);
            }
        }
    };
});

angular.module('app').directive('nowcamImg', function(){
    return {
        restrict: 'A',
        scope: {
            url: '=url'
        },
        link: function link(scope, element, attrs) {

            var inputFile = document.createElement('input');
            inputFile.setAttribute('type', 'file');
            // var theImg = document.getElementById('theImg');
            var theImg = element.append('<img/>')[0];
            theImg.onclick = function(){
                inputFile.click();
            }
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var img = new Image();
            var url;
            img.onload = function(){
                URL.revokeObjectURL(url);
                canvas.width = img.width;
                canvas.height = img.height;
                // console.log('img WxH: ', img.width, img.height);
                // console.log('canvas WxH: ', canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0);
                var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                grayscale(imageData);
                ctx.putImageData(imageData, 0, 0);
                theImg.src = canvas.toDataURL();
                scope.url = theImg.src;
                scope.$apply();
            }
            inputFile.onchange = function(){
                url = URL.createObjectURL(inputFile.files[0]);
                img.src = url;
            }

            function grayscale(imageData) {
                var data = imageData.data;
                for (var i = 0; i < data.length; i += 4) {
                    var avg = (data[i] + data[i +1] + data[i +2]) / 3;
                    data[i]     = avg; // red
                    data[i + 1] = avg; // green
                    data[i + 2] = avg; // blue
                }
            }

            // img.src = url;
        }
    }
});

angular.module('app').directive('nowcamCrop', function(){
    return {
        restrict: 'A',
        scope: {
            x: '=',
            y: '=',
            w: '=',
            h: '=',
        },
        link: function link(scope, element, attrs) {
            var el = element.children('img')[0];
            if(el.width > 0){
                el.Jcrop({
                    onSelect: function(c){
                        scope.x = c.x;
                        scope.y = c.y;
                        scope.w = c.w;
                        scope.h = h;
                        scope.$apply();
                    }
                })
            }
        }
    }
});

/*
*   LoginCtrl
*/
angular.module('app').controller('LoginCtrl', 
    ['$scope', '$window', '$state', 'AuthService', 
    function($scope, $window, $state, AuthService){

    $scope.email = undefined;
    $scope.password = undefined;

    $scope.login = function(){
        AuthService.login($scope.username, $scope.password).then(function(){
            console.log('successful login, goto customers');
            // $location.url('customers');
            $state.go('main.customers');
        }, function(err){
            $window.alert(err);
        })
    }

}]);

/*
*   PhotosApi
*/
angular.module('app').service('Photo', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
    }
    var cfg = {
        baseUrl: '/photos',
    }

    return {

        getAll: function(event_id, skip, limit, code){
            var params = {
                limit: limit,
                skip: skip, 
                event_id: event_id
            }
            if(code){
                params['code'] = code;
            }
            return $http.get(cfg.baseUrl, {params: params}, httpCfg);
        },

        /**
            photos - a object with 'items' property containing an array of photo objects
        */
        bulkUpdate: function(photos){
            return $http.put(cfg.baseUrl, photos);
        }
    }
}])

/*
*  Router
*/
angular.module('app').config(['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/login');

    $stateProvider

    .state('login', {
        url: '/login',
        templateUrl: 'static/partials/login.html',
        controller: 'LoginCtrl'
    })
    .state('main', {
        views: {
            '': {
                templateUrl: 'static/partials/main.html'
            }
        }
    })
    .state('main.customers', {
        url: '/customers',
        views: {
            '': {
                templateUrl: 'static/partials/customers.html',
                controller: 'CustomersCtrl'
            },
            search: {
                templateUrl: 'static/partials/search.html',
                controller: 'CustomersSearchCtrl'
            }
        }
    })
    .state('customers_new', {
        url: '/customers/new',
        templateUrl: 'static/partials/customers.new.html',
        controller: 'CustomersNewCtrl'
    })
    .state('customers_details', {
        url: '/customers/:id',
        templateUrl: 'static/partials/customers.details.html',
        controller: 'CustomersDetailsCtrl'
    })
    .state('units', {
        url: '/units',
        templateUrl: 'static/partials/units.html',
        controller: 'UnitsCtrl'
    })
    .state('units_new', {
        url: '/units/new',
        templateUrl: 'static/partials/units.new.html',
        controller: 'UnitsNewCtrl'
    })
    .state('units_details', {
        url: '/units/:id',
        templateUrl: 'static/partials/units.details.html',
        controller: 'UnitsDetailsCtrl'
    })
    .state('main.events', {
        url: '/events',
        views: {
            '': {
                templateUrl: 'static/partials/events.html',
                controller: 'EventsCtrl'
            },
            search: {
                templateUrl: 'static/partials/search.html',
                controller: 'EventsSearchCtrl'
            }
        }
    })
    .state('events_new', {
        url: '/events/new',
        templateUrl: 'static/partials/events.new.html',
        controller: 'EventsNewCtrl'
    })
    .state('events_details', {
        url: '/events/:id',
        templateUrl: 'static/partials/events.details.html',
        controller: 'EventsDetailsCtrl'
    })
    .state('events_details_photos', {
        url: '/events/:id/photos',
        templateUrl: 'static/partials/events.photos.html',
        controller: 'EventsPhotosCtrl'
    })
    .state('events_details_tickets', {
        url: '/events/:id/tickets',
        templateUrl: 'static/partials/events.tickets.html',
        controller: 'EventsTicketsCtrl'
    })
    .state('events_details_microsite', {
        url: '/events/:id/microsite',
        templateUrl: 'static/partials/events.microsite.html',
        controller: 'EventsMicrositeCtrl'
    })
    .state('events_details_stat', {
        url: '/events/:id/stat',
        templateUrl: 'static/partials/events.stat.html',
        controller: 'EventsStatCtrl'
    })
    .state('events_details_overlay', {
        url: '/events/:id/overlay',
        templateUrl: 'static/partials/events.overlay.html',
        controller: 'EventsOverlayCtrl'
    })
    .state('settings', {
        url: '/settings',
        templateUrl: 'static/partials/settings.html',
        controller: 'SettingsCtrl'
    })

}]);

/*
*   SettingsCtrl
*/
angular.module('app').controller('SettingsCtrl', 
    ['$scope', '$http', function($scope, $http){

    $scope.save = function(){
        $http({
            method: 'POST',
            url: '/settings',
            data: $scope.settings
        }).then(function(res){
            $scope.settings = res.data;
        }, function(err){
            console.error(err);
        });
    }

    function load(){
        $http.get('/settings').then(function(res){
            $scope.settings = res.data;
        }, function(err){
            console.error(err);
        });
    }

    load();

}]);
/*
*   UnitsApi
*/
angular.module('app').service('Unit', ['$http', function($http){

    var httpCfg = {
        timeout: 5000,
        // withCredentials: true
    }
    var cfg = {
        baseUrl: '/units',
        //loginId: 'john.smith',
        //tenantUrl: 'new.brightpattern.com'
    }

    return {

        getAll: function(skip, limit){
            return $http.get(cfg.baseUrl, {
                params: {
                    limit: limit,
                    skip: skip
                }
            }, httpCfg);
        },

        get: function(id){
            return $http.get(cfg.baseUrl + '/' + id, httpCfg);
        },

        create: function(event){
            return $http.post(cfg.baseUrl, event, httpCfg);
        },

        update: function(event){
            return $http.put(cfg.baseUrl, event, httpCfg);
        },

        delete: function(id){
            return $http.delete(cfg.baseUrl + '/' + id, httpCfg);
        }
    }
}])

/*
*   UnitsDetailsCtrl
*/
angular.module('app').controller('UnitsDetailsCtrl', 
    ['$scope', '$stateParams', 'Unit', function($scope, $stateParams, Unit){

    $scope.edit = function(){
        $scope.editable = true;
    }

    $scope.submit = function(){
        $scope.editable = false;
        Unit.update($scope.unit).then(function(res){
            //alert('ok!');
        }, function(err){
            $scope.editable = false;
            $scope.error = err;
        })
    }

    $scope.cancel = function(){
        // console.dir($scope.theForm);
        $scope.theForm.$rollbackViewValue();
        $scope.theForm.$setPristine();
        $scope.editable = false;
    }

    Unit.get($stateParams.id).then(function(resp){
        $scope.unit = resp.data;
    }, function(err){
        $scope.error = err;
    })


}]);


/*
*   UnitsCtrl
*/
angular.module('app').controller('UnitsCtrl', 
    ['$scope', '$state', 'Unit', function($scope, $state, Unit){

    var limit = 10;

    $scope.units = {
        items: [],
        total: 0,
        skip: 0
    };

    $scope.onAddUnitBtnClicked = function(){
        $state.go('units_new');
    }

    Unit.getAll($scope.skip, limit).then(function(res){
        $scope.units.items = res.data.items;
        $scope.units.total = res.data.total;
    }, function(err){
        console.error(err);
    });

}]);


/*
*   UnitsNewCtrl
*/
angular.module('app').controller('UnitsNewCtrl', 
    ['$scope', '$state', 'Unit', function($scope, $state, Unit){

    $scope.submit = function(){
        Unit.create($scope.unit).then(function(res){
            $state.go('units_details', {id: res.data._id});
        }, function(err){
            console.dir(err);
            $scope.error = err;
        })
    }

}]);


