# coding=UTF-8

from flask_oauthlib.client import OAuth
from .models import Settings


def get_settings():
    return Settings.get()


oauth = OAuth()

def get_twitter_oauth():
    if not 'twitter' in oauth.remote_apps:
        settings = get_settings()
        oauth.remote_app('twitter',
            base_url='https://api.twitter.com/1.1/',
            request_token_url='https://api.twitter.com/oauth/request_token',
            access_token_url='https://api.twitter.com/oauth/access_token',
            authorize_url='https://api.twitter.com/oauth/authenticate',
            # consumer_key='E0HZFfJ2yXoW2bCTiAj44HebR',
            # consumer_secret='EtrmhWNobchAjlOASgRVLtDl2ifn7K8a2vAfQRQdLGimDGOgS7'
            consumer_key=settings['twitter_app_id'],
            consumer_secret=settings['twitter_app_secret']
        )
    return oauth.twitter


def get_facebook_oauth():
    if not 'facebook' in oauth.remote_apps:
        settings = get_settings()
        oauth.remote_app('facebook',
            base_url='https://graph.facebook.com/',
            request_token_url=None,
            access_token_url='/oauth/access_token',
            authorize_url='https://www.facebook.com/dialog/oauth',
            # consumer_key='1682806698656275',                    # Test App One - Test1
            # consumer_secret='7939a24807f03a17c2127afe5828b38e',
            consumer_key=settings['facebook_app_id'],
            consumer_secret=settings['facebook_app_secret'],
            request_token_params={'scope': 'publish_actions'}
        )
    return oauth.facebook








