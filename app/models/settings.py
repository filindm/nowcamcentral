# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging
from .base import Base


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Settings(Base):

    _col = db['settings']

    _fields = [
        '_id',
        'site_url',
        'facebook_app_id',
        'facebook_app_secret',
        'twitter_app_id',
        'twitter_app_secret',
        'smtp_from',
        'smtp_host',
        'smtp_port',
        'smtp_user',
        'smtp_password',
        'smtp_ssl'
    ]


    @classmethod
    def get(cls):
        if cls._col.count() == 0:
            cls._col.insert({})
        return cls._col.find()[0]


    @classmethod
    def update(cls, src):
        doc = cls._get_fields(src)
        id = cls.get()['_id']
        if '_id' in doc:
            del doc['_id']
        r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': doc})
        if r.matched_count == 0:
            raise Exception(cls.__name__ + ' with id ' + id + ' was not found')
        return cls._col.find_one(ObjectId(id))

