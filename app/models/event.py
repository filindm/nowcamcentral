# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging
from PIL import Image, ImageOps
from io import BytesIO
from .base import Base


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Event(Base):

    _col = db['events']

    _fields = [
        '_id',
        'name',
        'customer',
        'code',
        'first_photo_taken',
        'last_photo_taken',
        'address',
        'description',
        'active',
        'photo_description',
        'start_time',
        'end_time',
        # 'fb_page',
        'fb_album',
        'fb_access_token',
        # 'fb_post_to_feed',
        # 'fb_autoupload',
        'cover_image_watermark',
        'cover_image_album_cover',
        'microsite_css',
        'microsite_subheader',
        # 'microsite_logo',
        # 'microsite_bkg',
        'microsite_data_fields',
        'microsite_data_text',
        'microsite_sharing_facebook',
        'microsite_sharing_twitter',
        'microsite_sharing_instagram',
        'microsite_sharing_emails',
        'microsite_sharing_text',
        'microsite_sharing_email_subject',
        'microsite_sharing_email_body',
        'microsite_sharing_twitter_text',
        'microsite_location_and_date',
        'microsite_privacy_policy',
        # 'ticket_body',
        'ticket_top',
        'ticket_left',
        'ticket_width',
        'ticket_height'
    ]


    @classmethod
    def get_all(cls, skip=0, limit=100, name=None, only_active=False, customer=None):
        spec = {u'name': {'$regex': name, '$options': 'i'}} if name else {}
        if only_active:
            spec['active'] = True
        if customer:
            spec['customer'] = customer
        return [x for x in cls._col.find(spec, skip=skip, limit=limit, sort=[('name', 1)])]

    @classmethod
    def set_microsite_logo(cls, id, file):
        cls._set_file_property(id, 'microsite_logo', file)


    @classmethod
    def get_microsite_logo(cls, id):
        return cls._get_file_property(id, 'microsite_logo')


    @classmethod
    def set_microsite_bkg(cls, id, file):
        cls._set_file_property(id, 'microsite_bkg', file)


    @classmethod
    def get_microsite_bkg(cls, id):
        return cls._get_file_property(id, 'microsite_bkg')


    @classmethod
    def set_ticket_body(cls, id, file):
        cls._set_file_property(id, 'ticket_body', file)


    @classmethod
    def get_ticket_body(cls, id):
        return cls._get_file_property(id, 'ticket_body')


    @classmethod
    def set_overlay(cls, id, file):
        cls._set_file_property(id, 'overlay', file)


    @classmethod
    def get_overlay(cls, id):
        return cls._get_file_property(id, 'overlay')


################################################################################
class EventUserData(Base):

    _col = db['events_user_data']

    _fields = [
        '_id',
        'event',
        'user_data'
    ]
