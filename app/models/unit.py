# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging
from PIL import Image, ImageOps
from io import BytesIO
from .base import Base


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Unit(object):

    _col = db['units']

    _fields = [
        '_id',
        'name',
        'mac',
        'event',
        'code',
        'description',
        'active'
    ]

    @classmethod
    def _get_fields(cls, src):
        return  { f: src[f] for f in cls._fields if f in src}


    @classmethod
    def get(cls, id):
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def find(cls, spec):
        return cls._col.find(spec)


    @classmethod
    def get_all(cls, skip=0, limit=100):
        return [x for x in cls._col.find(skip=skip, limit=limit)]


    @classmethod
    def create(cls, src):
        r = cls._col.insert_one(cls._get_fields(src))
        return cls._col.find_one(r.inserted_id)


    @classmethod
    def update(cls, src):
        doc = cls._get_fields(src)
        id = doc['_id']
        del doc['_id']
        r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': doc})
        if r.matched_count == 0:
            raise Exception('Unit with id ' + id + ' was not found')
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def count(cls):
        return cls._col.count()


