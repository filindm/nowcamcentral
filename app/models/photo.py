# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging
from PIL import Image, ImageOps
from io import BytesIO
from .base import Base


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Photo(Base):
    
    _col = db['photos']

    _fields = [
        '_id',
        'name',
        'event',
        'unit',
        'code',
        'stat',
        'hidden',
        'uuid'
    ]

    @classmethod
    def get_all(cls, event_id, skip=0, limit=100, code=None, show_hidden=False):
        spec = {'event': event_id}
        if code not in (None, ''):
            spec['code'] = code
        if not show_hidden:
            spec['hidden'] = {'$ne': True}
        return [x for x in cls._col.find(spec, skip=skip, limit=limit).sort('_id', -1)]


    @classmethod
    def update_stat(cls, uuid, channel):
        # r = cls._col.update_one({'_id': ObjectId(id)}, {'$inc': {'stat': {channel: 1}}})
        r = cls._col.update_one({'uuid': uuid}, {'$inc': {'stat.' + channel: 1}})
        if r.matched_count == 0:
            raise Exception('Photo with uuid ' + uuid + ' was not found')


    @classmethod
    def get_stat(cls, event_id):
        try:
            stat = cls._col.aggregate([{
                "$match": {"event": event_id}
            },{
                "$group": {
                    "_id": None, 
                    "times_printed": {"$sum": "$stat.printed"},
                    "times_shared_fb": {"$sum": "$stat.facebook"},
                    "times_shared_tw": {"$sum": "$stat.twitter"},
                    "times_shared_em": {"$sum": "$stat.email"},
                    "times_downloaded": {"$sum": "$stat.downloaded"},
                }
            }]).next()
        except:
            stat = {
                "times_printed": 0,
                "times_shared_fb": 0,
                "times_shared_tw": 0,
                "times_shared_em": 0,
                "times_downloaded": 0,
            }
        try:
            del stat['_id']
        except:
            pass
        stat['photos_total'] = cls._col.count({'event': event_id})
        stat['photos_total_shown'] = cls._col.count({'event': event_id, 'hidden': {'$ne': True}})
        return stat


    @classmethod
    def count(cls, event_id, code='', count_hidden=False):
        spec = {'event': event_id}
        if code:
            spec['code'] = code
        if not count_hidden:
            spec['hidden'] = {'$ne': True}
        return cls._col.count(spec)


    @classmethod
    def set_image(cls, id, file):
        cls._set_file_property(id, 'image', file.read())
        file.seek(0)
        THUMBNAIL_SIZE = 214, 136
        buf = BytesIO()
        img = Image.open(file)
        img.thumbnail(THUMBNAIL_SIZE)
        img.save(buf, img.format)
        buf.seek(0)
        cls._set_file_property(id, 'thumbnail', buf)


    @classmethod
    def get_image(cls, id):
        img = cls._get_file_property(id, 'image')
        mime_type = Image.MIME[Image.open(img).format]
        img.seek(0)
        return img, mime_type


    @classmethod
    def set_video(cls, id, file):
        cls._set_file_property(id, 'video', file.read())


    @classmethod
    def get_video(cls, id):
        return cls._get_file_property(id, 'video'), 'video/mp4'


    @classmethod
    def get_thumbnail(cls, id):
        img = cls._get_file_property(id, 'thumbnail')
        mime_type = Image.MIME[Image.open(img).format]
        img.seek(0)
        return img, mime_type

