# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging
from PIL import Image, ImageOps
from io import BytesIO


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Customer(object):

    _col = db['customers']

    _fields = [
        '_id',
        'name',
        'contact_name',
        'contact_phone',
        'contact_email'
    ]

    @classmethod
    def _get_fields(cls, src):
        return  { f: src[f] for f in cls._fields if f in src}


    @classmethod
    def get(cls, id):
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def get_all(cls, skip=0, limit=100, name=None):
        spec = {u'name': {'$regex': name, '$options': 'i'}} if name else {}
        return [x for x in cls._col.find(spec, skip=skip, limit=limit, sort=[('name', 1)])]


    @classmethod
    def create(cls, src):
        r = cls._col.insert_one(cls._get_fields(src))
        return cls._col.find_one(r.inserted_id)


    @classmethod
    def update(cls, src):
        doc = cls._get_fields(src)
        id = doc['_id']
        del doc['_id']
        r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': doc})
        if r.matched_count == 0:
            raise Exception('Customer with id ' + id + ' was not found')
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def set_logo(cls, id, logo):
        buf = BytesIO()
        size = 52, 52
        ImageOps.fit(Image.open(logo), size, Image.ANTIALIAS).save(buf, 'JPEG')
        buf.seek(0)
        doc = cls._col.find_one(ObjectId(id))
        if doc is None:
            raise Exception('Customer with id ' + id + ' was not found')
        if doc.get('logo') is not None:
            fs.delete(doc['logo'])
        logo_id = fs.put(buf)
        r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': {'logo': logo_id}})
        if r.matched_count == 0:
            raise Exception('Updating customer logo failed, customer id: ' + id)


    @classmethod
    def get_logo(cls, id):
        doc = cls._col.find_one(ObjectId(id))
        if doc is None:
            raise Exception('Customer with id ' + id + ' was not found')
        logo_id = doc.get('logo')
        if logo_id is None:
            raise Exception('Logo for customer with id ' + id + ' was not found')
        return fs.get(logo_id)


    @classmethod
    def count(cls):
        return cls._col.count()

