# coding=UTF-8

from app import db, fs
from bson.objectid import ObjectId
import logging


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class Base(object):

    _fields = []
    _col = None

    @classmethod
    def _get_fields(cls, src):
        return  { f: src[f] for f in cls._fields if f in src}


    @classmethod
    def get(cls, id):
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def find(cls, spec, skip=0, limit=100):
        return [x for x in cls._col.find(spec, skip=skip, limit=limit)]


    @classmethod
    def get_all(cls, skip=0, limit=100):
        return [x for x in cls._col.find(skip=skip, limit=limit)]


    @classmethod
    def create(cls, src):
        r = cls._col.insert_one(cls._get_fields(src))
        return cls._col.find_one(r.inserted_id)


    @classmethod
    def update(cls, src):
        doc = cls._get_fields(src)
        id = doc['_id']
        del doc['_id']
        r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': doc})
        if r.matched_count == 0:
            raise Exception(cls.__name__ + ' with id ' + id + ' was not found')
        return cls._col.find_one(ObjectId(id))


    @classmethod
    def bulk_update(cls, src):
        bulkop = cls._col.initialize_unordered_bulk_op()
        for s in src:
            doc = cls._get_fields(s)
            id = doc['_id']
            del doc['_id']
            bulkop.find({'_id': ObjectId(id)}).update_one({'$set': doc})
        bulkop.execute()


    @classmethod
    def count(cls):
        return cls._col.count()


    @classmethod
    def _set_file_property(cls, id, prop_name, file):
        doc = cls._col.find_one(ObjectId(id))
        if doc is None:
            raise Exception(cls.__name__ + ' with id ' + id + ' was not found')
        if doc.get(prop_name) is not None:
            fs.delete(doc[prop_name])
        if file is not None:
            file_id = fs.put(file)
            r = cls._col.update_one({'_id': ObjectId(id)}, {'$set': {prop_name: file_id}})
        else:
            r = cls._col.update_one({'_id': ObjectId(id)}, {'$unset': {prop_name: 1}})
        if r.matched_count == 0:
            raise Exception('Updating ' + prop_name + ' failed, event id: ' + id)


    @classmethod
    def _get_file_property(cls, id, prop_name):
        logger.debug('_get_file_property, id: ' + str(id) + ', prop_name: ' + prop_name)
        doc = cls._col.find_one(ObjectId(id))
        if doc is None:
            raise Exception(cls.__name__ + ' with id ' + id + ' was not found')
        file_id = doc.get(prop_name)
        # logger.debug('file_id: ' + str(file_id))
        if file_id is None:
            return None
            # raise Exception('Cannot find ' + prop_name + ' for event with id ' + id)
        return fs.get(file_id)

