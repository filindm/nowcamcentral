# coding=UTF-8

from app import db
import logging
from .base import Base


################################################################################
logger = logging.getLogger(__name__)


################################################################################
class User(Base):

    _col = db['users']

    _fields = [
        '_id',
        'email',
        'password',
        'active',
        'confirmed_at',
        'roles',
        'details'
    ]

    _col.create_index(u'email', unique=True)
    _col.create_index(u'details.fb_id', unique=True, sparse=True)


    @classmethod
    def get_by_email(cls, email):
        try:
            return cls.find({u'email': email})[0]
        except:
            return None

    @classmethod
    def get_by_fb_id(cls, fb_id):
        try:
            return cls.find({u'details.fb_id': fb_id})[0]
        except:
            return None

