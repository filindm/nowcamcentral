import requests
import os


AUTH_HDR = {'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNTkzMDIxMGNkM2Q4YjE3ZWM4YTUyYjRjIiwiZXhwIjoxNjUyMTYxODAxfQ.Z8FUiURdsv5aLqS34AqBOC2wt5jsnhYVuu-z1xRfuRI'}
# NOWCAM_URL = 'https://nowcam.filindm.org'
NOWCAM_URL = 'http://localhost:5000'


def get_photos(photos_num):
    r = requests.get(
        'https://api.flickr.com/services/rest/',
        params={
            'format': 'json',
            'nojsoncallback': 1,
            'method': 'flickr.interestingness.getList',
            'api_key': '7617adae70159d09ba78cfec73c13be3',
            'per_page': photos_num,
            'page': 1
        })
    r = r.json()
    if r['stat'] != 'ok':
        raise Exception('Flickr returned error: %s' % r['message'])

    i = 0
    for p in r['photos']['photo']:
        i = i + 1
        url = 'http://farm%(farm_id)s.staticflickr.com/%(server_id)s/%(id)s_%(secret)s_b.jpg' % {
            'farm_id': p['farm'],
            'server_id': p['server'],
            'id': p['id'],
            'secret': p['secret']
        }
        r = requests.get(url, stream=True)
        r.raise_for_status()
        r.raw.decode_content = True
        yield {'title': p['title'], 'file': r.raw}
        # filename = '{}.jpeg'.format(p['id'])
        # path = os.path.join('pics', filename)
        # with open(path, 'wb') as f:
        #     for chunk in r:
        #         f.write(chunk)

def create_events(num):
    events_url = '{}/events'.format(NOWCAM_URL)
    for p in get_photos(num):
        r = requests.post(events_url, json={
            'name': p['title'], 
            'customer': '58fd73a4b2e29d0005d85365',
            'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac ipsum interdum, malesuada enim vel, fringilla tellus. Aliquam vitae tellus id dolor blandit lobortis ut a velit. Pellentesque blandit dolor vitae urna facilisis, at faucibus nisl sodales. Proin laoreet elit urna, at condimentum augue mollis sed. Nam auctor euismod eros, nec ultricies erat fringilla a. Morbi leo tellus, efficitur vitae tincidunt nec, porttitor sit amet lacus. Cras sit amet aliquam tellus, ac cursus erat. Sed imperdiet felis id porta hendrerit. Nullam vulputate euismod mi, in lobortis tellus fermentum sed. Duis dictum felis vitae nunc efficitur, vitae blandit dolor pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce consectetur sodales justo, eget fringilla ante imperdiet ut. In mauris sapien, ultricies sit amet est at, vehicula aliquam metus.'
        }, headers=AUTH_HDR)
        r.raise_for_status()
        event_id = r.json()['_id']
        logo_url = '{}/events/{}/microsite_logo'.format(NOWCAM_URL, event_id)
        files = {'microsite_logo': p['file']}
        r = requests.post(logo_url, files=files, headers=AUTH_HDR)
        r.raise_for_status()


def main():
    create_events(30)

if __name__ == '__main__':
    main()







