import os
from fabric.api import env
from fabric.api import cd
# from fabric.api import lcd
from fabric.api import run
# from fabric.api import local
from fabric.api import sudo
from fabric.api import put
#from fabric.contrib.files import exists
#from fabric.contrib.files import sed
# from fabric.contrib.project import upload_project
from fabric.contrib.files import upload_template
from fabric.contrib.files import exists
from fabric.contrib.project import rsync_project

env.hosts = ['nowcam']
env.use_ssh_config = True

# host_name = 'nowcam.nowmarketing.com.au'
host_name = 'nowcam'
project_name = 'nowcam'
project_dir = '/srv/www/{}'.format(project_name)


def prepare_server():
    sudo('apt-get update')
    sudo('apt-get install -y libapache2-mod-wsgi python-virtualenv build-essential')
    sudo('apt-get install -y apache2 rsync libjpeg-dev')   
    sudo('apt-get install -y mongodb-server')
    sudo('apt-get install -y python-dev')
    sudo('service apache2 reload')

# def deploy():

#     sudo('service apache2 stop')

#     # project_dir = '/srv/www/{}'.format(project_name)
#     sudo('mkdir -p {}'.format(project_dir))

#     with cd(project_dir):
#         put('app.wsgi', '.', use_sudo=True)
#         put('requirements.txt', '.', use_sudo=True)
#         put('config.py', '.', use_sudo=True)
#         # put('app', '.', use_sudo=True)
#         if not exists('env', use_sudo=True):
#             sudo('virtualenv env')
#         sudo('source env/bin/activate && pip install -r requirements.txt')

#     sudo('chown -R www-data:www-data {}'.format(project_dir))
    
#     apache_cfg_ctx = {
#         'hostname': host_name, 
#         'project_name': project_name
#     }
#     upload_template(
#         'apache.cfg', 
#         '/etc/apache2/sites-available/{}-ssl.conf'.format(project_name), 
#         context=apache_cfg_ctx, 
#         use_sudo=True)
#     # sudo('a2enmod ssl')
#     sudo('a2ensite {}-ssl'.format(project_name))
#     # sudo('service apache2 restart')
#     sudo('service apache2 start')

# def tmp_deploy():
#     sudo('service apache2 stop')
#     # project_dir = '/srv/www/{}'.format(project_name)
#     with cd(project_dir):
#         put('app/crl_api.py', 'app/', use_sudo=True)
#         # put('app/models.py', 'app/', use_sudo=True)
#         # put('app/static/js/app.js', 'app/static/js/app.js', use_sudo=True)
#         # put('app/static/tpl/call-details-report.html', 'app/static/tpl/call-details-report.html', use_sudo=True)
#         # put('app/views.py', 'app/', use_sudo=True)
#     sudo('service apache2 start')


def sync():
    # sudo('chown -R ubuntu:ubuntu ' + project_dir)
    rsync_project(
        remote_dir=project_dir, 
        local_dir='./',
        exclude=('*.pyc', '*.log', '.gitignore', 'env'))
    with cd(project_dir):
        if not exists('env', use_sudo=True):
            sudo('virtualenv env')
        sudo('env/bin/pip install -r requirements.txt')
    sudo('chown -R www-data:www-data ' + project_dir)
    apache_cfg_ctx = {
        'hostname': host_name, 
        'project_name': project_name
    }
    upload_template(
        'apache.cfg', 
        '/etc/apache2/sites-available/{}-ssl.conf'.format(project_name), 
        context=apache_cfg_ctx, 
        use_sudo=True)
    # sudo('a2enmod ssl')
    sudo('a2ensite {}-ssl'.format(project_name))
    # sudo('service apache2 restart')
    sudo('service apache2 start')


# def sync():
#     rsync_project(
#         remote_dir=project_dir, 
#         local_dir='./',
#         exclude=('*.pyc', '*.log', '.git', '.gitignore', 'env'))
#     with cd(project_dir):
#         sudo('env/bin/pip install -r requirements.txt')
#     sudo('chown -R www-data:www-data ' + project_dir)
#     sudo('service apache2 restart')


# def install_celery():
#     sudo('test -e /etc/init.d/celeryd && service celeryd stop || true')
#     sudo('id -u celery || adduser --system --group celery')
#     put('celeryd', '/etc/init.d/celeryd', use_sudo=True)
#     sudo('chmod 755 /etc/init.d/celeryd')
#     put('celeryd.conf', '/etc/default/celeryd', use_sudo=True)
#     sudo('chown root:root /etc/default/celeryd')
#     sudo('chmod 640 /etc/default/celeryd')
#     sudo('/usr/sbin/update-rc.d celeryd defaults')
#     sudo('service celeryd start')


