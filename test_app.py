#!env/bin/python

import sys
import unittest
import json
import app
app.app.config['MONGO_DBNAME'] = 'nowcam_test'
app.app.config['MONGO_URL'] = 'localhost'
app.app.config['TESTING'] = True
from app import init_db
init_db()
from app import views, models
import logging


class BaseTestCase(unittest.TestCase):

    # MONGO_DBNAME = 'nowcam'
    # MONGO_URL = 'localhost'

    def setUp(self):
        self.app = app.app.test_client()

    def tearDown(self):
        app.db.client.drop_database(app.app.config['MONGO_DBNAME'])


class PhotosTestCase(BaseTestCase):

    def test_photos_unauthorized(self):
        rv = self.app.get('/customers')
        self.assertEqual(rv.status_code, 401)
        self.assertEqual(rv.data, 'Authorization required')

    def test_upload_jpeg_photo(self):
        self._test_upload_photo('test/res/car.jpg', 'image/jpeg')

    def test_upload_gif_photo(self):
        self._test_upload_photo('test/res/dog.gif', 'image/gif')

    def _test_upload_photo(self, path, content_type):
        event_id = models.Event.create({'name': 'Test Event'}).get('_id')
        rv = self.app.post('/photos', data={
            'name': 'photo_1',
            'event': event_id,
            'unit': '123',
            'code': 'ABC123',
            'file': open(path, 'rb')
        })
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.headers['Content-type'], 'application/json')
        photo_id = json.loads(rv.data)['_id']
        # Test loading image
        rv = self.app.get('/photos/' + photo_id + '/image')
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.headers['Content-type'], content_type)
        # print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
        # print open(path, 'rb').read()[100]
        # print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
        self.assertEqual(len(open(path, 'rb').read()), len(rv.data))
        self.assertEqual(open(path, 'rb').read(), rv.data, msg='files are not equal')
        # self.assertEqual(open(path, 'rb').read(6), rv.data[:6], msg='files are not equal')
        if content_type == 'image/jpeg':
            # self.assertEqual(rv.data[:2], '\xff\xd8', 'thumbnail is not a valid GIF')
            self.assertTrue(self._is_jpeg(rv.data), 'image is not a valid JPEG')
        elif content_type == 'image/gif':
            # self.assertEqual(rv.data[:6], 'GIF89a')
            self.assertTrue(self._is_gif(rv.data), 'image is not a valid GIF')

        # Test loading thumbnail
        rv = self.app.get('/photos/' + photo_id + '/thumbnail')
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.headers['Content-type'], content_type)
        if content_type == 'image/jpeg':
            # self.assertEqual(rv.data[:2], '\xff\xd8', 'thumbnail is not a valid GIF')
            self.assertTrue(self._is_jpeg(rv.data), 'thumbnail is not a valid JPEG')
        elif content_type == 'image/gif':
            # self.assertEqual(rv.data[:6], 'GIF89a')
            self.assertTrue(self._is_gif(rv.data), 'thumbnail is not a valid GIF')

    def _is_jpeg(self, data):
        return data[:2] == '\xff\xd8' and data[-2:] == '\xff\xd9'

    def _is_gif(self, data):
        return data[:6] == 'GIF89a'


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('abc').setLevel(logging.DEBUG)
    unittest.main()


