#!env/bin/python

from cherrypy import wsgiserver
from app import app
from app import init_db
init_db()
from app import views, models


d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 5000), d)


if __name__ == '__main__':
    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()
