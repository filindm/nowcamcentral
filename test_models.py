#!env/bin/python

import unittest
import json
import app
app.app.config['MONGO_DBNAME'] = 'nowcam_test'
app.app.config['MONGO_URL'] = 'localhost'
app.app.config['TESTING'] = True
from app import init_db
init_db()
from app import models


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.app.test_client()

    def tearDown(self):
        app.db.client.drop_database(app.app.config['MONGO_DBNAME'])


class PhotosTestCase(BaseTestCase):

    def test_create_jpeg_photo(self):
        self._test_create_photo('test/res/car.jpg', 'image/jpeg')

    def test_create_gif_photo(self):
        self._test_create_photo('test/res/dog.gif', 'image/gif')

    def _test_create_photo(self, path, mime_type):
        event_id = models.Event.create({'name': 'Test Event'}).get('_id')
        photo = models.Photo.create({
            'name': 'photo_1',
            'event': event_id,
            'unit': '123',
            'code': 'ABC123'
        })
        self.assertIsNotNone(photo)
        self.assertEquals(photo['name'], 'photo_1')
        self.assertEquals(photo['event'], event_id)
        self.assertEquals(photo['unit'], '123')
        self.assertEquals(photo['code'], 'ABC123')
        models.Photo.set_image(photo['_id'], open(path, 'rb'))
        # Test image
        img, img_mime_type = models.Photo.get_image(photo['_id'])
        self.assertEqual(mime_type, img_mime_type)
        self.assertEquals(open(path, 'rb').read(), img.read(), 'stored image differs from original')
        # if mime_type == 'image/jpeg':
        #     self.assertTrue(self._is_jpeg(img.read()), 'image is not a valid JPEG')
        # elif mime_type == 'image/gif':
        #     self.assertTrue(self._is_gif(img.read()), 'image is not a valid GIF')
        # Test thumbnail
        img, img_mime_type = models.Photo.get_thumbnail(photo['_id'])
        self.assertEqual(mime_type, img_mime_type)
        if mime_type == 'image/jpeg':
            data = img.read()
            self.assertTrue(self._is_jpeg(data), 'thumbnail is not a valid JPEG: {0}...'.format(data[:10]))
        elif mime_type == 'image/gif':
            data = img.read()
            self.assertTrue(self._is_gif(data), 'thumbnail is not a valid GIF: {0}...'.format(data[:10]))

    def _is_jpeg(self, data):
        return data[:2] == '\xff\xd8' and data[-2:] == '\xff\xd9'

    def _is_gif(self, data):
        return data[:6] == 'GIF89a'



if __name__ == '__main__':
    unittest.main()



