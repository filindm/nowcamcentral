import os
import datetime

DEBUG = True

SECRET_KEY = 'NMljDXC0ikM9L6C6ulCx'

### JWT
JWT_EXPIRATION_DELTA = datetime.timedelta(days=360)
# JWT_VERIFY_EXPIRATION = False

### Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'stream': 'ext://sys.stdout'
        }
    },
    'root': {
        'handlers': ['console'],
        'level': 'DEBUG'
    },
    'nowcammediahub': {
        'handlers': ['console'],
        'level': 'DEBUG'
    },
}
