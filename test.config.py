import os
import datetime

APP_BASE_DIR = os.path.abspath(os.path.dirname(__file__))

APP_TITLE = 'Nowcam Central'
DEBUG = True
# API_ROOT_URI = '/api/v0.1'

SECRET_KEY = 'NMljDXC0ikM9L6C6ulCx'

### JWT
JWT_EXPIRATION_DELTA = datetime.timedelta(minutes=60)

### MongoDB
MONGO_DBNAME = 'nowcam_test'
MONGO_URL = 'localhost'

### Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'stream': 'ext://sys.stdout'
        }
    },
    'root': {
        'handlers': ['console'],
        'level': 'DEBUG'
    }
}
