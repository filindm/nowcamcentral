#!env/bin/python

import os
os.environ['NOWCAM_SETTINGS'] = os.path.abspath('config.debug.py')
import subprocess
import signal

from app import app
from app import init_db
init_db()
from app import views, models

p = subprocess.Popen('./watch')
app.run('0.0.0.0', debug=True)
p.send_signal(signal.SIGINT)
p.wait()
